from brownie import accounts, reverts, chain, network

from helpers.helpers import add_multiple_standard_members, get_member_contract_owner, expire_memberships
from helpers.helpers import go_to_recovery_period, get_member_contract, add_member_with_role, NULL_ADDRESS
from helpers.helpers import add_multiple_standard_members_with_invitation
from fixtures.contracts import *
import time

# possbile error messages
NOT_MEMBER_REQUEST_ERR_MSG = 'Requester is not a member'
MEMBER_NOT_EXIST = 'Member does not exist'
MEMBER_ALREADY_EXISTS = 'Member already exists'
ONLY_MEMBER_CAN_SEND_INVITATION_MSG = 'Only member can send invitation'
NO_INVITATION_ERR_MSG = 'No active invitation for this address'
INVALID_TIME_ERR_MSG = 'Time should be greater than 0'
ADMIN_OPERATION_ONLY_ERR_MSG = 'Admin operation only'
ADMIN_CAN_NOT_LEAVE_ORGANIZATION_ERR_MSG = 'Admin can not leave organization before chosing new one'
ADMIN_ROLE_CAN_NOT_BE_ASSIGNED_ERR_MSG = 'Admin role can not be assigned to any member without changing admin'
ADMIN_ROLE_CANNOT_BE_CHANGED_ERR_MSG = 'Admin role can not be chenged without chaning admin'
NO_ACCESS_RIGHTS_ERR_MSG = 'Member without access rights'
NO_RIGHTS_TO_DOWNGRADE_ROLE_ERR_MSG = 'No access rights to downgrade role'
SAME_ROLE_ERR_MSG = 'The same role'
INVITATION_INACTIVE_ERR_MSG = 'Invitation expired'
NOT_INVITER_REQUEST_ERR_MSG = 'No inviter can not cancel invitation'
ADD_ADMIN_MEMBER_ERR_MSG = 'Member with admin role can not be added'
ALREADY_ADMIN_ERR_MSG = 'Member is already an admin'
RECOVERY_LONGER_THAN_MEMBERHIP_PERIOD_ERR_MSG = 'Recovery time longer than membership period'


def test_get_members_no_members(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract

    members_list = contract.getMembers({'from' : contract_owner})
    assert len(members_list) == 0, 'Should return empty array; admin should be access separately'


def test_get_members_request_by_not_member(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.getMembers({'from' : accounts[10]})


def test_get_members_multiple_active_members(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract
    
    members_nums = [1, 2, 3]
    add_multiple_standard_members_with_invitation(contract, accounts_list=members_nums)
    
    # request by owner
    members_list = contract.getMembers({'from' : contract_owner})
    assert len(members_list) == len(members_nums), 'Should return the same number of member as was created; admin should be access separately and is not counted'

    # request by standard member
    members_list = contract.getMembers({'from' : accounts[1]})
    assert len(members_list) == len(members_nums), 'Should return the same number of member as was created; admin should be access separately and is not counted'


def test_get_members_multiple_members_but_not_all_members_are_active(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract
    
    members_nums = [1, 2, 3, 4]
    add_multiple_standard_members_with_invitation(contract, accounts_list=members_nums)
    expire_memberships(contract)

    new_members_nums = [5, 6]
    add_multiple_standard_members_with_invitation(contract, accounts_list=new_members_nums)

    members_list = contract.getMembers({'from' : contract_owner})
    members_list = contract.getMembers({'from' : accounts[5]})
    assert len(members_list) == len(new_members_nums), 'Should be equal to all member - ousted members'


def test_get_member_active_member(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    account_num = 1
    add_multiple_standard_members_with_invitation(contract, [account_num])

    member_contract_address = contract.getMember({'from' : accounts[account_num]})
    assert accounts[account_num] == get_member_contract_owner(member_contract_address), 'Only member contract owner can access member contract address'


def test_get_member_admin_member(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract

    member_contract_address = contract.getMember({'from': contract_owner})
    assert NULL_ADDRESS == member_contract_address, 'Admin can not be accessed through getMember(); it has its own method getAdmin()'


def test_get_member_not_member(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    add_multiple_standard_members_with_invitation(contract, [1, 2])
    
    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.getMember({'from' : accounts[10]})


def test_get_member_ousted_member(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    add_multiple_standard_members_with_invitation(contract, [1])
    
    # lower membership period to expire membership of existing members
    expire_memberships(contract)
    add_multiple_standard_members_with_invitation(contract, [2])

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG): # no longer a member when membership expired
        contract.getMember({'from' : accounts[1]})

    member_contract_address = contract.getMember({'from' : accounts[2]})
    assert accounts[2] == get_member_contract_owner(member_contract_address), 'Only member contract owner can access member contract address'


def test_check_if_membership_expired(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    add_multiple_standard_members_with_invitation(contract, [1])
    member_contract_address = contract.getMember({'from' : accounts[1]})
    
    expire_memberships(contract)
    assert contract.checkIfMembershipExpired(member_contract_address) is True

    add_multiple_standard_members_with_invitation(contract, [3])
    assert contract.checkIfMembershipExpired(contract.getMember({'from' : accounts[3]})) is False


def test_check_if_membership_expired_member_not_exist(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    add_multiple_standard_members_with_invitation(contract, [1, 2])

    with reverts(MEMBER_NOT_EXIST):
        contract.checkIfMembershipExpired(NULL_ADDRESS)


def test_check_if_member_belongs_to_organization(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract
    add_multiple_standard_members_with_invitation(contract, [1, 2])

    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[1]}) is True
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[2]}) is True

    assert contract.checkIfMemberBelongsToOrganization({'from': contract_owner}) is True, 'Admin also belongs to organization'


def test_check_if_member_belongs_to_organization_membership_expired(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    add_multiple_standard_members_with_invitation(contract, [1, 2])

    expire_memberships(contract)
    add_multiple_standard_members_with_invitation(contract, [3])

    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[1]}) is False
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[2]}) is False
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[3]}) is True


def test_invite_new_members(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    contract.invite(accounts[1], 3600, 'test@mail.com')  # by admin
    contract.invite(accounts[2], 3600, 'test@mail.com')  # by admin
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[1]}) is False, 'Should not belong yet'
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[2]}) is False, 'Should not belong yet'
    add_multiple_standard_members(contract, [1, 2])
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[1]}) is True, 'Should belong after addMember() was executed'
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[2]}) is True, 'Should belong after addMember() was executed'

    contract.invite(accounts[3], 3600, 'test@mail.com', {'from': accounts[2]})  # non-admin invitation
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[3]}) is False, 'Should not belong yet'
    add_multiple_standard_members(contract, [3])
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[3]}) is True, 'Should belong after addMember() was executed'


def test_invite_existing_members(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    new_member_account_num = 1

    contract.invite(accounts[new_member_account_num], 3600, 'test@mail.com')  # by admin
    add_multiple_standard_members(contract, [new_member_account_num])

    with reverts(MEMBER_ALREADY_EXISTS):
        contract.invite(accounts[new_member_account_num], 3600, 'test@mail.com')


def test_invite_not_member_sends_invitation(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.invite(accounts[1], 3600, 'test@mail.com', {'from': accounts[10]}), 'Only admin can send first invitation'


def test_invite_send_invitation_to_admin(central_planned_organization_contract):
    contract, admin = central_planned_organization_contract

    with reverts(MEMBER_ALREADY_EXISTS):
        contract.invite(admin, 3600, 'test@mail.com')


def test_cancel_invitation(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    invitee_account_addr = accounts[2]
    contract.invite(invitee_account_addr, 3600, 'test@mail.com')
    contract.cancelInvitation(invitee_account_addr)

    with reverts(NO_INVITATION_ERR_MSG):
        contract.addMember('', '', '', '', {'from': invitee_account_addr})


def test_cancel_invitation_not_inviter_request(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    add_multiple_standard_members_with_invitation(contract, [1, 2])

    invitee_account_addr = accounts[3]
    contract.invite(invitee_account_addr, 3600, 'test@mail.com')
    
    with reverts(NOT_INVITER_REQUEST_ERR_MSG): # not inviter
        contract.cancelInvitation(invitee_account_addr, {'from': accounts[1]})

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG): # not even a member
        contract.cancelInvitation(invitee_account_addr, {'from': accounts[10]})


def test_cancel_invitation_no_invitation(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    contract.invite(accounts[1], 3600, 'test@mail.com')
    with reverts():
        contract.cancelInvitation(accounts[8], {'from': accounts[10]})


def test_check_if_invited(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    invitee_account_addr = accounts[1]
    contract.invite(invitee_account_addr, 3600, 'test@mail.com')
    
    assert contract.checkIfInvited(invitee_account_addr) is True
    assert contract.checkIfInvited(accounts[10]) is False


def test_check_if_invited_invitation_expired(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    invitee_account_addr = accounts[1]
    contract.invite(invitee_account_addr, 1, 'test@mail.com')
    
    # expire invitation
    time.sleep(2)
    network.chain.mine()
    assert contract.checkIfInvited(invitee_account_addr) is False


def test_add_member_success(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    new_member_account = accounts[1]

    contract.invite(new_member_account, 3600, '')
    contract.addMember('name', 'surname', '3@mail.com', 'nick', {'from' : new_member_account})

    # check if member exists and by who was created
    member_contract_addr = contract.getMember({'from' : new_member_account})
    assert get_member_contract_owner(member_contract_addr) == new_member_account.address.lower()



def test_add_member_after_membership_expired(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    add_multiple_standard_members_with_invitation(contract, [1, 2, 3])
    expire_memberships(contract)

    active_members = contract.getMembers()
    assert len(active_members) == 0, 'All member expired'
    
    add_multiple_standard_members_with_invitation(contract, [2, 5])
    active_members = contract.getMembers()
    assert len(active_members) == 2, '1 members added again + 1 new members'


def test_add_member_deleted_member(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract

    add_multiple_standard_members_with_invitation(contract, [2, 3, 4])
    member_contract_addr = contract.getMember({'from': accounts[4]})
    
    contract.deleteMember(member_contract_addr, {'from': contract_owner})
    active_members = contract.getMembers()
    assert len(active_members) == 2, 'one member was removed by admin'

    add_multiple_standard_members_with_invitation(contract, [4])    
    active_members = contract.getMembers()
    assert len(active_members) == 3, 'user added once again'


def test_add_member_without_invitation(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    with reverts(NO_INVITATION_ERR_MSG):
        contract.addMember('name', 'surname', '3@mail.com', 'nick')


def test_set_member_role(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract
    
    admin_account_addr = contract_owner
    editor_account_addr = accounts[1]
    standard_member_account_1 = accounts[2]
    standard_member_account_2 = accounts[3]

    admin_role = 0
    editor_role = 1
    standard_role = 2

    add_member_with_role(contract, editor_account_addr, editor_role)
    add_member_with_role(contract, standard_member_account_1, standard_role)
    add_member_with_role(contract, standard_member_account_2, standard_role)

    with reverts(NO_ACCESS_RIGHTS_ERR_MSG): # standard member has no privileges
        contract.setMemberRole(standard_member_account_1, editor_role, {'from': standard_member_account_2})

    # change standard user role by editor
    contract.setMemberRole(standard_member_account_1, editor_role, {'from': editor_account_addr})
    member_contract_addr = contract.getMember({'from': standard_member_account_1})
    assert get_member_contract(member_contract_addr).getRole() == editor_role, 'Editor is able to upgrade user role'
    with reverts(NO_RIGHTS_TO_DOWNGRADE_ROLE_ERR_MSG):
        contract.setMemberRole(standard_member_account_1, standard_role, {'from': editor_account_addr}), 'Editor can not downgrade another editor role'

    # change standard user role by admin
    contract.setMemberRole(standard_member_account_2, editor_role, {'from': admin_account_addr})
    member_contract_addr = contract.getMember({'from': standard_member_account_2})
    assert get_member_contract(member_contract_addr).getRole() == editor_role, 'Admin is able to upgrade user role'
    
    contract.setMemberRole(standard_member_account_2, standard_role, {'from': admin_account_addr})
    member_contract_addr = contract.getMember({'from': standard_member_account_2})
    assert get_member_contract(member_contract_addr).getRole() == standard_role, 'Admin is able to downgrade user role'


def test_set_member_role_invalid_data(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract
    
    admin_account_addr = contract_owner
    editor_account_addr = accounts[1]
    standard_member_account_1 = accounts[2]
    standard_member_account_2 = accounts[3]

    admin_role = 0
    editor_role = 1
    standard_role = 2

    add_member_with_role(contract, editor_account_addr, editor_role)
    add_member_with_role(contract, standard_member_account_1, standard_role)
    add_member_with_role(contract, standard_member_account_2, standard_role)

    with reverts(ADMIN_ROLE_CAN_NOT_BE_ASSIGNED_ERR_MSG):
        contract.setMemberRole(standard_member_account_1, admin_role, {'from': editor_account_addr})

    with reverts(ADMIN_ROLE_CAN_NOT_BE_ASSIGNED_ERR_MSG):
        contract.setMemberRole(standard_member_account_1, admin_role, {'from': admin_account_addr})

    with reverts(ADMIN_ROLE_CANNOT_BE_CHANGED_ERR_MSG):
        contract.setMemberRole(admin_account_addr, editor_role, {'from': admin_account_addr})

    with reverts(MEMBER_NOT_EXIST):
        contract.setMemberRole(NULL_ADDRESS, editor_role, {'from': editor_account_addr})

    with reverts(SAME_ROLE_ERR_MSG):
        contract.setMemberRole(standard_member_account_2, standard_role, {'from': editor_account_addr})


def test_change_admin(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract

    prev_admin_account_addr = contract_owner
    new_admin_account_addr = accounts[2]

    add_member_with_role(contract, new_admin_account_addr, role=2)
    contract.changeAdmin(new_admin_account_addr, {'from': prev_admin_account_addr})

    prev_admin_member_contract = get_member_contract(contract.getMember({'from': prev_admin_account_addr}))
    assert prev_admin_member_contract.getOwner() == prev_admin_account_addr, 'Previous admin can be access as a regular member from now'
    assert prev_admin_member_contract.getRole() == 2, 'Previous admin is now standard member'

    new_admin_member_contract = get_member_contract(contract.getAdmin())
    assert new_admin_member_contract.getOwner() == new_admin_account_addr
    assert new_admin_member_contract.getRole() == 0
    assert NULL_ADDRESS == contract.getMember({'from': new_admin_account_addr}), 'New admin can not be access as regular user from now' 


def test_change_admin_new_admin_is_not_member(central_planned_organization_contract):
    contract, admin_account_addr = central_planned_organization_contract

    with reverts(MEMBER_NOT_EXIST):
        contract.changeAdmin(accounts[2], {'from': admin_account_addr})


def test_change_admin_not_admin(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    with reverts(ADMIN_OPERATION_ONLY_ERR_MSG):
        contract.changeAdmin(accounts[2], {'from': accounts[10]})


def test_change_admin_already_admin(central_planned_organization_contract):
    contract, admin_account_addr = central_planned_organization_contract

    with reverts(ALREADY_ADMIN_ERR_MSG):
        contract.changeAdmin(admin_account_addr, {'from': admin_account_addr})


def test_update_member(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    add_multiple_standard_members_with_invitation(contract, [1, 2])

    new_name = 'update name'
    new_surnname = 'updated surname'
    new_nick = 'updated nick'

    contract.updateMember(new_name, new_surnname, new_nick, {'from': accounts[1]})
    member_contract = get_member_contract(contract.getMember({'from': accounts[1]}))
    assert member_contract.getName() == new_name
    assert member_contract.getSurname() == new_surnname
    assert member_contract.getNickname() == new_nick    


def test_update_member_no_member_owner(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    
    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.updateMember('', '', '', {'from': accounts[10]})


def test_delete_member_exists(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract
    
    add_multiple_standard_members_with_invitation(contract, [1, 2])
    member_contract_address = contract.getMember({'from': accounts[1]})

    assert member_contract_address != NULL_ADDRESS
    contract.deleteMember(member_contract_address, {'from': contract_owner})

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):   # no longer a member
        contract.getMember({'from': accounts[1]})


def test_delete_member_not_exists(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract
    
    add_multiple_standard_members_with_invitation(contract, [1, 2])

    member_addr_to_delete = accounts[3]
    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):   # not member for sure
        contract.getMember({'from': member_addr_to_delete})

    with reverts(MEMBER_NOT_EXIST):
        contract.deleteMember(member_addr_to_delete, {'from': contract_owner})


def test_delete_member_no_admin_request(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    
    add_multiple_standard_members_with_invitation(contract, [1, 2])

    member_to_delete = contract.getMember({'from': accounts[1]})
    with reverts(ADMIN_OPERATION_ONLY_ERR_MSG):
        contract.deleteMember(member_to_delete, {'from': accounts[2]})


def test_renew_membership(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    owner_account_num = 1
    add_multiple_standard_members_with_invitation(contract, [owner_account_num])
    
    old_timestamp = contract.getMembershipStartTime({'from' : accounts[owner_account_num]})
    go_to_recovery_period(contract)
    contract.renewMembership({'from': accounts[owner_account_num]})
    new_timestamp = contract.getMembershipStartTime({'from' : accounts[owner_account_num]})

    assert new_timestamp > old_timestamp, 'The start time should be updated'


def test_renew_membership_not_member(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    member_account_num = 1
    not_member_account_num = 2
    add_multiple_standard_members_with_invitation(contract, [member_account_num])
    
    go_to_recovery_period(contract)

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.renewMembership({'from': accounts[not_member_account_num]})


def test_renew_membership_expired(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    member_account_num = 1
    add_multiple_standard_members_with_invitation(contract, [member_account_num])
    
    expire_memberships(contract)

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.renewMembership({'from': accounts[member_account_num]})


def test_get_membership_start_time(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    add_multiple_standard_members_with_invitation(contract, [1, 2])
    assert contract.getMembershipStartTime({'from': accounts[1]}) > 0
    assert contract.getMembershipStartTime({'from': accounts[2]}) > 0

    assert contract.getMembershipStartTime({'from': accounts[1]}) < chain[-1].timestamp
    assert contract.getMembershipStartTime({'from': accounts[2]}) == chain[-1].timestamp


def test_get_membership_start_period_by_not_member(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    owner_account_num = 1
    add_multiple_standard_members_with_invitation(contract, [owner_account_num])

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.getMembershipStartTime({'from': accounts[3]})


def test_get_admin(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract

    admin_member_contract_addr = contract.getAdmin()
    assert contract_owner == get_member_contract_owner(admin_member_contract_addr), 'Organization should have member contract created by admin'

    # not admin also should be able to receive data
    add_multiple_standard_members_with_invitation(contract, [1])
    admin_member_contract_addr = contract.getAdmin({'from': accounts[1]})
    assert contract_owner == get_member_contract_owner(admin_member_contract_addr)


def test_get_admin_requester_is_not_member(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.getAdmin({'from': accounts[10]})


def test_update(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract

    new_name = 'new name'
    new_desc = 'new desciption'
    new_membership_period = 2000
    new_recovery_period = 1000

    # by admin
    contract.update(new_name, new_desc, new_membership_period, new_recovery_period, {'from': contract_owner})
    organization_details = contract.getOrganizationDetails()
    assert new_name == organization_details[0]
    assert new_desc == organization_details[1]
    assert new_membership_period == organization_details[2]
    assert new_recovery_period == organization_details[3]

    # by editor
    editor_account_addr = accounts[2]
    add_member_with_role(contract, editor_account_addr, 1)

    new_name = 'new name editor'
    new_desc = 'new desciption editor'
    new_membership_period = 2222
    new_recovery_period = 1111
    contract.update(new_name, new_desc, new_membership_period, new_recovery_period, {'from': editor_account_addr})
    organization_details = contract.getOrganizationDetails()
    assert new_name == organization_details[0]
    assert new_desc == organization_details[1]
    assert new_membership_period == organization_details[2]
    assert new_recovery_period == organization_details[3]


def test_update_change_in_membership_period_should_not_affect_existing_members(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    add_multiple_standard_members_with_invitation(contract, [1, 2, 3])
    expire_memberships(contract)
    assert len(contract.getMembers()) == 0, 'All members expired'

    new_big_membership_period = 100000000
    contract.update('', '', new_big_membership_period, 100)
    assert len(contract.getMembers()) == 0, 'Extending membership period should not re-actiate expired memberships'

    add_multiple_standard_members_with_invitation(contract, [1, 2, 3, 4])
    assert len(contract.getMembers()) == 4

    new_low_membership_period = 2
    time.sleep(new_low_membership_period+1)
    contract.update('', '', new_low_membership_period, new_low_membership_period-1)
    assert len(contract.getMembers()) == 4, 'Lowering membership time should not expire existing members - they received compensation'


def test_update_invalid_data(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract

    new_name = 'new_name'
    new_desc = 'new desciption'
    new_membership_period = 100 # invalid - recovery > membership
    new_recovery_period = 20000

    with reverts(RECOVERY_LONGER_THAN_MEMBERHIP_PERIOD_ERR_MSG):
        contract.update(new_name, new_desc, new_membership_period, new_recovery_period, {'from': contract_owner})

    new_membership_period = 0 # invalid - should be greater than 0
    new_recovery_period = 0

    with reverts(INVALID_TIME_ERR_MSG):
        contract.update(new_name, new_desc, new_membership_period, new_recovery_period, {'from': contract_owner})

    new_membership_period = 10000
    new_recovery_period = 0 # invalid - should be greater than 0

    with reverts(INVALID_TIME_ERR_MSG):
        contract.update(new_name, new_desc, new_membership_period, new_recovery_period, {'from': contract_owner})


def test_update_requester_without_access_rights(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    add_multiple_standard_members_with_invitation(contract, [1, 2])

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG): # not member call
        contract.update('', '', 10000, 10000, {'from': accounts[10]})

    with reverts(NO_ACCESS_RIGHTS_ERR_MSG): # standard member call
        contract.update('', '', 10000, 10000, {'from': accounts[1]})


def test_leave_organization(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract
    member_nums = [1, 2, 3]
    member_to_leave = accounts[2]

    add_multiple_standard_members_with_invitation(contract, member_nums)
    contract.leaveOrganization({'from': member_to_leave})

    assert len(contract.getMembers()) == len(member_nums) - 1
    assert contract.checkIfMemberBelongsToOrganization({'from': member_to_leave}) is False


def test_leave_organization_admin(central_planned_organization_contract):
    contract, contract_owner = central_planned_organization_contract

    with reverts(ADMIN_CAN_NOT_LEAVE_ORGANIZATION_ERR_MSG): # admin can't leave organization before chosing new one
        contract.leaveOrganization({'from': contract_owner})


def test_leave_organization_not_member(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.leaveOrganization({'from': accounts[10]})


def test_get_organization_details():
    organization_name = 'test_name_dao'
    organization_desc = 'desc...'
    membership_period = 1111
    recovery_period = 222
    organization_type = 'CENTRAL_PLANNED_ORGANIZATION'

    contract = CentralPlannedOrganization.deploy(organization_name, organization_desc, membership_period, recovery_period, {'from' : accounts[0]})
    
    organization_details = contract.getOrganizationDetails()
    assert organization_name == organization_details[0]
    assert organization_desc == organization_details[1]
    assert membership_period == organization_details[2]
    assert recovery_period == organization_details[3]
    assert organization_type == organization_details[4]


def test_get_organization_details_not_member_request(central_planned_organization_contract):
    contract, _ = central_planned_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.getOrganizationDetails({'from': accounts[10]})
