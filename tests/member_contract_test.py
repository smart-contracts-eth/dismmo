from brownie import accounts
from fixtures.contracts import member_contract


def test_set_name_by_organization(member_contract):
    new_name = 'new_test_name'

    member_contract.setName(new_name)  # called from the same account as the one which created a member
    name_after_update = member_contract.getName()

    assert name_after_update == new_name


def test_set_name_invalid_organization_address(member_contract):
    not_organization_account = accounts[3]
    new_name = 'new_test_name'
    
    member_contract.setName(new_name, {'from' : not_organization_account})
    name_after_update = member_contract.getName()

    assert name_after_update != new_name


def test_set_email_by_organization(member_contract):
    new_email = 'new_test@mail.com'

    member_contract.setEmail(new_email)
    email_after_update = member_contract.getEmail()

    assert email_after_update == new_email


def test_set_email_invalid_organization_address(member_contract):
    not_organization_account = accounts[3]
    new_email = 'new_test@mail.com'

    member_contract.setEmail(new_email, {'from' : not_organization_account})
    email_after_update = member_contract.getEmail()

    assert email_after_update != new_email


def test_set_surname_by_organization(member_contract):
    new_surname = 'new_test_surname'

    member_contract.setSurname(new_surname)
    surname_after_update = member_contract.getSurname()

    assert surname_after_update == new_surname


def test_set_surname_invalid_organization_address(member_contract):
    not_organization_account = accounts[3]
    new_surname = 'new_test_surname'

    member_contract.setSurname(new_surname, {'from' : not_organization_account})
    surname_after_update = member_contract.getSurname()

    assert surname_after_update != new_surname


def test_set_nickname_by_organization(member_contract):
    new_nickname = 'new_test_nickname'

    member_contract.setNickname(new_nickname)
    nickname_after_update = member_contract.getNickname()

    assert nickname_after_update == new_nickname


def test_set_nickname_invalid_organization_address(member_contract):
    not_organization_account = accounts[3]
    new_nickname = 'new_test_nickname'

    member_contract.setNickname(new_nickname, {'from' : not_organization_account})
    nickname_after_update = member_contract.getNickname()

    assert nickname_after_update != new_nickname


def test_set_role_by_organization(member_contract):
    new_role = 0

    member_contract.setRole(new_role)
    role_after_update = member_contract.getRole()

    assert role_after_update == new_role


def test_set_role_invalid_organization_address(member_contract):
    not_organization_account = accounts[3]
    new_role = 0

    member_contract.setRole(new_role, {'from' : not_organization_account})
    role_after_update = member_contract.getRole()

    assert role_after_update != new_role
