from brownie import Voting, BasicMockContract, accounts, reverts, network
from fixtures.contracts import *
import time


OPTIONS_NUMBER_NOT_EQUAL_TO_PAYLOAD_ERR = 'Options number is different than payloads number'
TOO_SHORT_VOTING_TIME_ERR = 'Too short voting time'
VOTING_PERIOD_EXPIRED_ERR = 'Voting period expired'
INVALID_OPTION_ERR = 'Invalid option'
VOTING_NOT_STARTED_ERR = 'Voting not started yet'
VOTING_NOT_FINISHED_ERR = 'Voting not finished yet'
NOT_VOTING_OWNER_ERR = 'Not voting owner'
VOTING_ALREADY_STARTED_ERR = 'Voting already started'
USER_ALREADY_VOTED_ERR = 'User already voted'
USER_NOT_ALLOWED_FOR_VOTING_ERR_MSG = 'User cannot vote in this voting - created before user was added'


def test_deploy_contract_different_number_of_payloads_than_payloads():
    basic_mock_contract = BasicMockContract.deploy({'from': accounts[0]})
    callback_mock = basic_mock_contract.setValue.encode_input(100)

    with reverts(OPTIONS_NUMBER_NOT_EQUAL_TO_PAYLOAD_ERR):
        Voting.deploy('test', ['option1', 'option2'], basic_mock_contract, [callback_mock], 20, callback_mock, {'from' : accounts[0]})


def test_start_voting(voting_contract):
    voting_contract, _ = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]
    
    voting_contract.startVoting(voters, 1000)


def test_start_voting_not_voting_owner(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]

    with reverts(NOT_VOTING_OWNER_ERR):
        voting_contract.startVoting(voters, 1000, {'from' : accounts[10]})

    voting_contract.startVoting(voters, 1000, {'from': owner})


def test_start_voting_negative_voting_time(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]
    
    with reverts(TOO_SHORT_VOTING_TIME_ERR):
        voting_contract.startVoting(voters, 0, {'from' : owner})


def test_start_voting_already_started(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]
    
    voting_contract.startVoting(voters, 1000, {'from': owner})
    with reverts(VOTING_ALREADY_STARTED_ERR):
        voting_contract.startVoting(voters, 1000, {'from': owner})


def test_check_if_voting_time_ended(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]

    voting_time = 1
    voting_contract.startVoting(voters, voting_time, {'from' : owner})
    assert voting_contract.checkIfVotingTimeEnded() is False
    
    time.sleep(voting_time + 1)
    network.chain.mine()
    assert voting_contract.checkIfVotingTimeEnded() is True


def test_check_if_voted_option_exists(voting_contract):
    voting_contract, _ = voting_contract

    for option in range(0, 3):
        assert voting_contract.checkIfVotedOptionExists(option) == True

    assert voting_contract.checkIfVotedOptionExists(3) == False


def test_vote(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]

    voting_contract.startVoting(voters, 1000, {'from': owner})
    voting_contract.vote(0, {'from': voters[0]})
    voting_contract.vote(1, {'from': voters[1]})


def test_vote_voting_not_started(voting_contract):
    voting_contract, _ = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]

    with reverts(VOTING_NOT_STARTED_ERR):
        voting_contract.vote(0, {'from': voters[0]})


def test_vote_voting_option_not_existst(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]

    voting_contract.startVoting(voters, 1000, {'from': owner})
    with reverts(INVALID_OPTION_ERR):
        voting_contract.vote(10, {'from': voters[0]})


def test_vote_not_voter(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]

    voting_contract.startVoting(voters, 1000, {'from': owner})
    with reverts(USER_NOT_ALLOWED_FOR_VOTING_ERR_MSG):
        voting_contract.vote(0, {'from': accounts[10]})


def test_vote_voting_time_expired(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]

    voting_time = 2
    voting_contract.startVoting(voters, voting_time, {'from': owner})
    voting_contract.vote(0, {'from': voters[0]})

    time.sleep(voting_time + 1)
    network.chain.mine()
    with reverts(VOTING_PERIOD_EXPIRED_ERR):
        voting_contract.vote(0, {'from': voters[1]})


def test_vote_user_already_voted(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[3]]

    voting_contract.startVoting(voters, 1000, {'from': owner})
    voting_contract.vote(0, {'from': voters[1]})
    with reverts(USER_ALREADY_VOTED_ERR):
        voting_contract.vote(1, {'from' : voters[1]})


def test_has_user_voted(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1]]

    voting_contract.startVoting(voters, 1000, {'from': owner})
    assert voting_contract.hasUserVoted({'from': owner}) is False, 'User has not voted yet'
    voting_contract.vote(0, {'from': owner})
    assert voting_contract.hasUserVoted({'from': owner}) is True
    

def test_has_user_voted_not_organization_member(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1]]

    voting_contract.startVoting(voters, 1000, {'from': owner})
    assert voting_contract.hasUserVoted({'from': accounts[10]}) is False, 'User without voting rights cannot vote'


def test_finalize_voting():
    mocked_contract = BasicMockContract.deploy({'from': accounts[0]})
    mocked_function_call_yes = mocked_contract.setValue.encode_input(100)
    mocked_function_call_no = mocked_contract.setValue.encode_input(-100)
    mocked_callback = mocked_contract.getValue.encode_input()
    
    contract_owner = accounts[0]
    voting_contract = Voting.deploy('Test Voting', ['Yes', 'No'], mocked_contract, [mocked_function_call_yes, mocked_function_call_no], 60, mocked_callback, {'from' : contract_owner})
    voters = [contract_owner, accounts[1], accounts[2]]

    voting_time = 2
    voting_contract.startVoting(voters, voting_time, {'from': contract_owner})
    voting_contract.vote(0, {'from': voters[0]})

    time.sleep(voting_time + 1)
    voting_contract.finalizeVoting({'from': contract_owner})
    assert voting_contract.checkIfVotingIsFinalized() is True
    assert mocked_contract.getValue() != 100, 'Not enough votes were given to execute callback'

    # new voting
    voting_contract = Voting.deploy('Test Voting', ['Yes', 'No'], mocked_contract, [mocked_function_call_yes, mocked_function_call_no], 60, mocked_callback, {'from' : contract_owner})
    
    voting_time = 3
    voting_contract.startVoting(voters, voting_time, {'from': contract_owner})
    voting_contract.vote(1, {'from': voters[1]})
    voting_contract.vote(1, {'from': voters[2]})

    time.sleep(voting_time + 1)
    voting_contract.finalizeVoting({'from': contract_owner})
    assert voting_contract.checkIfVotingIsFinalized() is True
    assert mocked_contract.getValue() == -100, 'Exceeded turnout threshold - voting results should be applied'


def test_finalize_voting_not_voting_owner(voting_contract):
    voting_contract, _ = voting_contract
    not_owner = accounts[10]

    with reverts(NOT_VOTING_OWNER_ERR):
        voting_contract.finalizeVoting({'from': not_owner})


def test_finalize_voting_voting_not_started(voting_contract):
    voting_contract, owner = voting_contract

    with reverts(VOTING_NOT_STARTED_ERR):
        voting_contract.finalizeVoting({'from': owner})


def test_finalize_voting_voting_still_active(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1]]

    voting_contract.startVoting(voters, 1000, {'from': owner})
    with reverts(VOTING_NOT_FINISHED_ERR):
        voting_contract.finalizeVoting({'from': owner})


def test_get_voting_result_voting_finalized(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1], accounts[2], accounts[3], accounts[4]]

    voting_contract.startVoting(voters, 4, {'from': owner})
    voting_contract.vote(0, {'from': voters[2]})
    voting_contract.vote(1, {'from': voters[3]})
    voting_contract.vote(1, {'from': voters[4]})

    time.sleep(5)
    network.chain.mine()

    voting_contract.finalizeVoting({'from': owner})
    voting_result = voting_contract.getVotingResult({'from' : voters[2]})
    
    option_name, count, _ = voting_result[0]
    assert option_name == 'Yes' and count == 1, '1 member voted for the first option'

    option_name, count, _ = voting_result[1]
    assert option_name == 'No' and count == 2, '2 member voted for the second option'


def test_get_voting_result_voting_finished_but_not_finalized(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1]]

    voting_time = 2
    voting_contract.startVoting(voters, voting_time, {'from': owner})
    voting_contract.vote(0, {'from': voters[1]})

    time.sleep(voting_time + 1)
    network.chain.mine()

    with reverts(VOTING_NOT_FINISHED_ERR):
        voting_contract.getVotingResult({'from' : accounts[2]})


def test_get_voting_result_voting_not_started(voting_contract):
    voting_contract, _ = voting_contract

    with reverts(VOTING_NOT_FINISHED_ERR):
        voting_contract.getVotingResult({'from' : accounts[2]})


def test_get_voting_result_voting_is_still_active(voting_contract):
    voting_contract, owner = voting_contract
    voters = [accounts[0], accounts[1]]

    voting_contract.startVoting(voters, 1000, {'from': owner})
    with reverts(VOTING_NOT_FINISHED_ERR):
        voting_contract.getVotingResult()
