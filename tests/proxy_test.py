from brownie import Contract, DecentralizedAutonomousOrganization, CentralPlannedOrganization, accounts, reverts
from fixtures.contracts import *
import json


def test_call_proxy_dao():
    organization_contract_name = 'DecentralizedAutonomousOrganization'
    organization_contract = DecentralizedAutonomousOrganization.deploy('test_name', '', 3600, 600, 200, 50, {'from': accounts[0]})
    organization_proxy = OrganizationProxy.deploy({'from': accounts[0]})
    organization_proxy.updateOrganizationAddress(organization_contract.address)

    with open(f"build/contracts/{organization_contract_name}.json") as f:
        contract_abi = json.load(f)["abi"]
    contract_address = organization_proxy.address

    organization_contract_instance = Contract.from_abi(organization_contract_name, contract_address, contract_abi)
    assert organization_contract_instance.getOrganizationType({'from': accounts[0]}) == 'DECENTRALIZED_AUTONOMOUS_ORGANIZATION', 'Now proxy should work as organization contract'

    # try to create some transaction
    organization_contract_instance.invite(accounts[10], 100, '', {'from': accounts[0]})
    assert organization_contract_instance.checkIfInvited(accounts[10], {'from': accounts[0]}) is True


def test_call_proxy_central_planned_organization():
    organization_contract_name = 'CentralPlannedOrganization'
    organization_contract = CentralPlannedOrganization.deploy('test_name', '', 3600, 600, {'from': accounts[0]})
    organization_proxy = OrganizationProxy.deploy({'from': accounts[0]})
    organization_proxy.updateOrganizationAddress(organization_contract.address)

    with open(f"build/contracts/{organization_contract_name}.json") as f:
        contract_abi = json.load(f)["abi"]
    contract_address = organization_proxy.address

    organization_contract_instance = Contract.from_abi(organization_contract_name, contract_address, contract_abi)
    assert organization_contract_instance.getOrganizationType({'from': accounts[0]}) == 'CENTRAL_PLANNED_ORGANIZATION', 'Now proxy should work as organization contract'

    # try to create some transaction
    organization_contract_instance.invite(accounts[10], 100, '', {'from': accounts[0]})
    assert organization_contract_instance.checkIfInvited(accounts[10], {'from': accounts[0]}) is True


def test_call_proxy_receive_revert_message():
    organization_contract_name = 'CentralPlannedOrganization'
    organization_contract = CentralPlannedOrganization.deploy('test_name', '', 3600, 600, {'from': accounts[0]})
    organization_proxy = OrganizationProxy.deploy({'from': accounts[0]})
    organization_proxy.updateOrganizationAddress(organization_contract.address)

    with open(f"build/contracts/{organization_contract_name}.json") as f:
        contract_abi = json.load(f)["abi"]
    contract_address = organization_proxy.address

    organization_contract_instance = Contract.from_abi(organization_contract_name, contract_address, contract_abi)
    with reverts('No active invitation for this address'):
        organization_contract_instance.addMember('', '', '', '', {'from': accounts[10]}) # addMember() without invitatiton


def test_update_proxy_not_owner_request():
    organization_contract = DecentralizedAutonomousOrganization.deploy('test_name', '', 3600, 600, 200, 50, {'from': accounts[0]})
    organization_proxy = OrganizationProxy.deploy({'from': accounts[0]})
    
    with reverts("Only owner can change organization address"):
        organization_proxy.updateOrganizationAddress(organization_contract.address, {'from': accounts[10]})
