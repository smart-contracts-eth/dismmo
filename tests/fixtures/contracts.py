from brownie import Member, Voting, CentralPlannedOrganization, DecentralizedAutonomousOrganization, BasicMockContract
from brownie import OrganizationProxy, accounts, network
from helpers.helpers import get_voting_contract
import pytest
import time


@pytest.fixture
def central_planned_organization_contract():
    contract_owner = accounts[0]
    contract = CentralPlannedOrganization.deploy('test_name_central', 'desc...', 3600, 600, {'from' : contract_owner})
    return contract, contract_owner


@pytest.fixture
def dao_organization_contract():
    contract_owner = accounts[0]
    contract = DecentralizedAutonomousOrganization.deploy('test_name_dao', 'desc...', 3600, 600, 2, 5, {'from' : contract_owner})
    return contract, contract_owner


@pytest.fixture
def dao_organization_contract_with_multiple_members():
    members_accounts = [accounts[0], accounts[1], accounts[2], accounts[3]]

    voting_time = 2
    contract = DecentralizedAutonomousOrganization.deploy('test_name_dao', 'desc...', 3600, 600, voting_time, 1, {'from' : members_accounts[0]})

    # first member invited and added without voting
    contract.invite(members_accounts[1], 100, '', {'from': members_accounts[0]})
    contract.addMember('mem1', 'mem1', 'mem1@mail.com', 'mem1', {'from': accounts[1]})

    # vote for inviting users
    voting_contracts = []
    inviter = members_accounts[0]
    for member_account_addr in members_accounts[2:]:
        contract.invite(member_account_addr, 100, '', {'from': inviter})

        invite_voting = contract.getVotings()[-1]
        voting_contract = get_voting_contract(invite_voting)
        voting_contract.vote(0, {'from': inviter})  # vote for yes

        voting_contracts.append(voting_contract)

    time.sleep(voting_time)
    network.chain.mine()
    for voting_contract in voting_contracts:
        voting_contract.finalizeVoting({'from': inviter})

    # add members with passed invite voting
    for member_account in members_accounts[2:]:
        contract.addMember('mem', 'mem', 'mem@mail.com', 'mem', {'from': member_account})

    return contract, members_accounts, voting_time


@pytest.fixture
def member_contract():
    return Member.deploy('test', 'test', 'test@mail.com', 't', 2, {'from' : accounts[0]})


@pytest.fixture
def voting_contract():
    mocked_contract = BasicMockContract.deploy({'from': accounts[0]})
    mocked_function_call = mocked_contract.setValue.encode_input(100)

    contract_owner = accounts[0]
    contract = Voting.deploy('Test Voting', ['Yes', 'No', 'Anyway'], mocked_contract, [mocked_function_call, mocked_function_call, mocked_function_call], 60, mocked_function_call, {'from' : contract_owner})
    return contract, contract_owner


@pytest.fixture
def voting_contract_with_organization():
    dao_org_contract = DecentralizedAutonomousOrganization.deploy('test_name', '', 3600, 600, 10, 20, {'from': accounts[0]})
    org_function_call = dao_org_contract.getOrganizationType.encode_input()
    renew_membership_callback = dao_org_contract.renewMembership.encode_input()

    contract_owner = accounts[0]
    vot_contract = Voting.deploy('Test Voting', ['Yes', 'No'], dao_org_contract, [org_function_call, org_function_call], 20, renew_membership_callback, {'from' : contract_owner})
    return vot_contract, dao_org_contract, contract_owner


@pytest.fixture
def organization_proxy_contract():
    return OrganizationProxy.deploy({'from' : accounts[0]})
