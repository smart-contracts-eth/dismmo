// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract BasicMockContract {
    int public value = 10;

    function setValue(int _value) external {
        value = _value;
    }

    function getValue() external view returns (int) {
        return value;
    }
}
