#!/bin/bash

# Scirpt should be executed from a main project directory
current_dir=$(pwd)
if [[ "$current_dir" != */dismmo ]]; then
    echo "Error: This script must be run from a main project directory (*/dismmo)."
    exit 1
fi

SOURCE_DIR="./tests/mocks"
DEST_DIR="./contracts"
BUILD_DIR="./build"

# Save file names from source directory to an array
file_list=($(find "$SOURCE_DIR" -type f -exec basename {} \;)) &&

# Copy mocked contracts to compile them
for file in "${file_list[@]}"; do
    cp "$SOURCE_DIR/$file" "$DEST_DIR"
done

# Run test
brownie test --network hardhat;

# Clean contracts and build from mocks
for file in "${file_list[@]}"; do
    rm "$DEST_DIR/$file"
    rm "${BUILD_DIR}/contracts/${file%.*}.json"
done

echo "Tests completed. Check results above"
