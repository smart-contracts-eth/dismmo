from brownie import accounts, reverts, chain, DecentralizedAutonomousOrganization

from helpers.helpers import get_member_contract_owner, get_member_contract, NULL_ADDRESS
from helpers.helpers import finalize_last_voting_with_option, dao_expire_memberships
from fixtures.contracts import *


NOT_MEMBER_REQUEST_ERR_MSG = 'Requester is not a member'
MEMBER_NOT_EXIST = 'Member does not exist'
MEMBER_ALREADY_EXISTS = 'Member already exists'
ONLY_MEMBER_CAN_SEND_INVITATION_MSG = 'Only member can send invitation'
NO_INVITATION_ERR_MSG = 'No active invitation for this address'
INVALID_TIME_ERR_MSG = 'Time should be greater than 0'
INVALID_VOTING_TIME_ERR_MSG = 'Voting time is too low'
INVALID_TURNOUT_THRESHOLD_ERR_MSG = 'Turnout threshold should be percentage value(0-100)'
INVALID_CALLBACK_CALL_ERR_MSG = 'Invalid callback call - voting not found'
RECOVERY_PERIOD_LONGER_THAN_MEMBERHIP_PERIOD_ERR_MSG = 'Recovery time longer than membership period'


def test_deploy_DAO():
    contract_owner = accounts[0]
    contract = DecentralizedAutonomousOrganization.deploy('test_name_dao', 'desc...', 3600, 600, 100, 2, {'from' : contract_owner})
    
    assert len(contract.getMembers()) == 1, 'Organization should have one member(founder) after deploy'
    assert get_member_contract_owner(contract.getMember({'from': contract_owner})) == contract_owner, 'This single member is the one who created organization'


def test_deploy_DAO_invalid_params():
    contract_owner = accounts[0]
    
    with reverts(INVALID_VOTING_TIME_ERR_MSG):
        DecentralizedAutonomousOrganization.deploy('test_name_dao', 'desc...', 3600, 600, 0, 2, {'from' : contract_owner}), 'too low voting time'

    with reverts(INVALID_TURNOUT_THRESHOLD_ERR_MSG):
        DecentralizedAutonomousOrganization.deploy('test_name_dao', 'desc...', 3600, 600, 100, 120, {'from' : contract_owner}), '100 percent exceeded'

    with reverts(RECOVERY_PERIOD_LONGER_THAN_MEMBERHIP_PERIOD_ERR_MSG):
        DecentralizedAutonomousOrganization.deploy('test_name_dao', 'desc...', 10, 1000, 200, 20, {'from' : contract_owner})


def test_get_organization_details():
    organization_name = 'test_name_dao'
    organization_desc = 'desc...'
    membership_period = 1111
    recovery_period = 222
    voting_time = 999
    turnout_threshold = 55
    organization_type = 'DECENTRALIZED_AUTONOMOUS_ORGANIZATION'

    contract = DecentralizedAutonomousOrganization.deploy(organization_name, organization_desc, membership_period, recovery_period, voting_time, turnout_threshold, {'from' : accounts[0]})
    
    organization_details = contract.getOrganizationDetails()
    assert organization_name == organization_details[0]
    assert organization_desc == organization_details[1]
    assert membership_period == organization_details[2]
    assert recovery_period == organization_details[3]
    assert voting_time == organization_details[4]
    assert turnout_threshold == organization_details[5]
    assert organization_type == organization_details[6]


def test_get_organization_details_not_member_request(dao_organization_contract):
    contract, _ = dao_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.getOrganizationDetails({'from': accounts[10]})


def test_update_without_voting(dao_organization_contract):
    contract, _ = dao_organization_contract

    organization_details = contract.getOrganizationDetails()
    old_name = organization_details[0]
    old_desc = organization_details[1]
    old_membership_period = organization_details[2]
    old_recovery_period = organization_details[3]
    old_voting_time = organization_details[4]
    old_turnout_threshold = organization_details[5]

    new_name = 'new_name'
    new_desc = 'new desciption'
    new_membership_period = 2222
    new_recovery_period = 1111
    new_voting_time = 111
    new_turnout_threshold = 66

    assert new_name != old_name
    assert new_desc != old_desc
    assert new_membership_period != old_membership_period
    assert new_recovery_period != old_recovery_period
    assert new_voting_time != old_voting_time
    assert new_turnout_threshold != old_turnout_threshold

    contract.update(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold)

    organization_details = contract.getOrganizationDetails()
    assert new_name == organization_details[0]
    assert new_desc == organization_details[1]
    assert new_membership_period == organization_details[2]
    assert new_recovery_period == organization_details[3]
    assert new_voting_time == organization_details[4]
    assert new_turnout_threshold == organization_details[5]


def test_update_voting_time_was_changed(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members

    organization_details = contract.getOrganizationDetails()
    old_name = organization_details[0]
    old_desc = organization_details[1]
    old_membership_period = organization_details[2]
    old_recovery_period = organization_details[3]
    old_turnout_threshold = organization_details[5]

    # set low voting time
    new_voting_time = 2
    voter = members_accounts[0]
    contract.update(old_name, old_desc, old_membership_period, old_recovery_period, new_voting_time, old_turnout_threshold, {'from': voter})
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voter, option=0)
    assert new_voting_time == contract.getOrganizationDetails()[4]

    # set new high voting time to check if voting takes new amount of time, not the prevoius one
    contract.update(old_name, old_desc, old_membership_period, old_recovery_period, 100000, old_turnout_threshold)
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voter, option=0)
    contract.invite(accounts[10], 100, 'test@mail.com', {'from': members_accounts[0]})

    time.sleep(new_voting_time + 1)  # voting with prevous time would have expired
    network.chain.mine()

    voting_contract = get_voting_contract(contract.getVotings()[-1])
    assert voting_contract.checkIfVotingTimeEnded() is False, 'Voting should not expire after applying new higher votingTime'


def test_update_with_voting(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members

    organization_details = contract.getOrganizationDetails()
    old_name = organization_details[0]
    old_desc = organization_details[1]
    old_membership_period = organization_details[2]
    old_recovery_period = organization_details[3]
    old_voting_time = organization_details[4]
    old_turnout_threshold = organization_details[5]

    new_name = 'new_name'
    new_desc = 'new desciption'
    new_membership_period = 2222
    new_recovery_period = 1111
    new_voting_time = 111
    new_turnout_threshold = 66

    assert new_name != old_name
    assert new_desc != old_desc
    assert new_membership_period != old_membership_period
    assert new_recovery_period != old_recovery_period
    assert new_voting_time != old_voting_time
    assert new_turnout_threshold != old_turnout_threshold

    voter = members_accounts[0]
    contract.update(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': voter})
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voter, option=1)

    # voted against changes, so non of the params should be changed
    organization_details = contract.getOrganizationDetails()
    assert old_name == organization_details[0]
    assert old_desc == organization_details[1]
    assert old_membership_period == organization_details[2]
    assert old_recovery_period == organization_details[3]
    assert old_voting_time == organization_details[4]
    assert old_turnout_threshold == organization_details[5]

    contract.update(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': voter})
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voter, option=0)

    # voted for changes, so params should be updated
    organization_details = contract.getOrganizationDetails()
    assert new_name == organization_details[0]
    assert new_desc == organization_details[1]
    assert new_membership_period == organization_details[2]
    assert new_recovery_period == organization_details[3]
    assert new_voting_time == organization_details[4]
    assert new_turnout_threshold == organization_details[5]


def test_update_invalid_data(dao_organization_contract):
    contract, contract_owner = dao_organization_contract

    new_name = 'new_name'
    new_desc = 'new desciption'
    new_membership_period = 0  # invalid - should be greater than 0
    new_recovery_period = 0
    new_voting_time = 100
    new_turnout_threshold = 20

    with reverts(INVALID_TIME_ERR_MSG):
        contract.update(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': contract_owner})

    new_membership_period = 10000
    new_recovery_period = 0  # invalid - should be greater than 0

    with reverts(INVALID_TIME_ERR_MSG):
        contract.update(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': contract_owner})

    new_membership_period = 100
    new_recovery_period = 200  # invalid - should less than membership period

    with reverts(RECOVERY_PERIOD_LONGER_THAN_MEMBERHIP_PERIOD_ERR_MSG):
        contract.update(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': contract_owner})

    new_membership_period = 200
    new_recovery_period = 100
    new_voting_time = 0  # invalid - should be greater than 0

    with reverts(INVALID_VOTING_TIME_ERR_MSG):
        contract.update(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': contract_owner})

    new_voting_time = 100
    new_turnout_threshold = 250  # invalid - exceeded 100 percent

    with reverts(INVALID_TURNOUT_THRESHOLD_ERR_MSG):
        contract.update(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': contract_owner})


def test_update_requester_is_not_member(dao_organization_contract):
    contract, _ = dao_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.update('', '', 100, 10, 100, 20, {'from': accounts[10]})


def test_get_members(dao_organization_contract):
    contract, contract_owner = dao_organization_contract

    members_list = contract.getMembers({'from' : contract_owner})
    assert len(members_list) == 1, 'Organization alawys contains one member(founder) after deployment'

    new_member_account = accounts[10]
    contract.invite(new_member_account, 100, '', {'from': contract_owner})
    contract.addMember('', '', '', '', {'from': new_member_account})  # it can be done without voting because only one member in organization

    assert len(members_list) + 1 == len(contract.getMembers({'from' : contract_owner})), 'One more member was added, so one more should be returned'


def test_get_members_request_by_not_member(dao_organization_contract):
    contract, _ = dao_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.getMembers({'from' : accounts[10]})


def test_get_member_active_member(dao_organization_contract):
    contract, contract_owner = dao_organization_contract

    member_contract_address = contract.getMember({'from' : contract_owner})
    assert contract_owner == get_member_contract_owner(member_contract_address), 'Only member contract owner can access member contract address'


def test_get_member_not_member(dao_organization_contract):
    contract, _ = dao_organization_contract
    
    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.getMember({'from' : accounts[10]})


def test_get_member_ousted_member(dao_organization_contract):
    contract, contract_owner = dao_organization_contract
    new_member_account = accounts[2]
    contract.invite(new_member_account, 1000, 'test@mail.com')

    # lower membership period to expire membership of existing members
    dao_expire_memberships(contract)

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG): # no longer a member when membership expired
        contract.getMember({'from' : contract_owner})

    contract.addMember('', '', '', '', {'from': new_member_account})
    member_contract_address = contract.getMember({'from' : accounts[2]})
    assert new_member_account == get_member_contract_owner(member_contract_address), 'Only member contract owner can access member contract address'


def test_check_if_membership_expired(dao_organization_contract):
    contract, contract_owner = dao_organization_contract
    new_member_account = accounts[3]
    contract.invite(new_member_account, 1000, 'test@mail.com')

    dao_expire_memberships(contract)
    contract.addMember('', '', '', '', {'from': new_member_account})

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.checkIfMembershipExpired(contract.getMember({'from' : contract_owner})), 'No longer a member'
    
    member_contract_addr = contract.getMember({'from' : new_member_account})
    assert contract.checkIfMembershipExpired(member_contract_addr, {'from': new_member_account}) is False


def test_check_if_membership_expired_member_not_exist(dao_organization_contract):
    contract, _ = dao_organization_contract

    with reverts(MEMBER_NOT_EXIST):
        contract.checkIfMembershipExpired(NULL_ADDRESS)


def test_check_if_member_belongs_to_organization(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members

    for member_account in members_accounts:
        assert contract.checkIfMemberBelongsToOrganization({'from': member_account}) is True


def test_check_if_member_belongs_to_organization_membership_expired(dao_organization_contract):
    contract, contract_owner = dao_organization_contract
    new_member_account = accounts[3]
    contract.invite(new_member_account, 1000, 'test@mail.com')

    dao_expire_memberships(contract)
    contract.addMember('', '', '', '', {'from': new_member_account})

    assert contract.checkIfMemberBelongsToOrganization({'from': contract_owner}) is False
    assert contract.checkIfMemberBelongsToOrganization({'from': new_member_account}) is True


def test_check_if_invited(dao_organization_contract):
    contract, _ = dao_organization_contract

    invitee_account_addr = accounts[1]
    contract.invite(invitee_account_addr, 3600, 'test@mail.com')
    
    assert contract.checkIfInvited(invitee_account_addr) is True
    assert contract.checkIfInvited(accounts[10]) is False, 'No invitation sent to this address'


def test_check_if_invited_invitation_expired(dao_organization_contract):
    contract, _ = dao_organization_contract

    invitee_account_addr = accounts[1]
    contract.invite(invitee_account_addr, 1, 'test@mail.com')
    
    # expire invitation
    time.sleep(2)
    network.chain.mine()
    assert contract.checkIfInvited(invitee_account_addr) is False


def test_invite_new_members(dao_organization_contract):
    contract, contract_owner = dao_organization_contract

    # first invitation without voting
    contract.invite(accounts[1], 3600, 'test@mail.com')
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[1]}) is False, 'Should not belong yet'

    contract.addMember('', '', '', '', {'from': accounts[1]})    
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[1]}) is True, 'Should belong after addMember() was executed'

    # second invtiation with voting
    contract.invite(accounts[2], 3600, 'test@mail.com')
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[2]}) is False, 'Should not belong yet'
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=contract_owner, option=1)  # vote for NO option
    with reverts(NO_INVITATION_ERR_MSG):
        contract.addMember('', '', '', '', {'from': accounts[2]}), 'members voted against inviting new member'

    contract.invite(accounts[2], 3600, 'test@mail.com')
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=contract_owner, option=0)  # vote for YES option
    contract.addMember('', '', '', '', {'from': accounts[2]})
    assert contract.checkIfMemberBelongsToOrganization({'from': accounts[2]}) is True, 'Should belong after addMember() was executed'


def test_invite_existing_members(dao_organization_contract):
    contract, contract_owner, = dao_organization_contract

    with reverts(MEMBER_ALREADY_EXISTS):
        contract.invite(contract_owner, 3600, 'test@mail.com')
    assert contract.checkIfInvited(contract_owner) is False


def test_invite_not_member_sends_invitation(dao_organization_contract):
    contract, _ = dao_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.invite(accounts[1], 3600, 'test@mail.com', {'from': accounts[10]}), 'Only member can send invitation'


def test_invite_callback(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members

    voting_owner = members_accounts[0]
    invetee = accounts[9]
    contract.invite(invetee, 100, '', {'from': voting_owner})
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voting_owner, option=0) # first callback is executed here

    with reverts(INVALID_CALLBACK_CALL_ERR_MSG):
        voting_addr = contract.getVotings()[-1]
        contract.inviteCallback(invetee, 100, '', {'from': voting_addr}), 'Callback action should be exectud only once'

    not_voting_owner_addr = accounts[10]
    with reverts(INVALID_CALLBACK_CALL_ERR_MSG):
        contract.inviteCallback(invetee, 100, '', {'from': not_voting_owner_addr}), 'Callback should fail if no voting was created'


def test_delete_member_callback(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members
    init_members_count = len(members_accounts)

    voting_owner = members_accounts[0]
    member_to_delete = contract.getMember({'from': members_accounts[1]})
    contract.deleteMember(member_to_delete, {'from': voting_owner})
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voting_owner, option=0)  # first callback is executed here

    assert len(contract.getMembers()) == init_members_count - 1

    with reverts(INVALID_CALLBACK_CALL_ERR_MSG):
        voting_addr = contract.getVotings()[-1]
        contract.deleteMemberCallback(member_to_delete, {'from': voting_addr}), 'Callback action should be exectud only once'

    not_voting_owner_addr = accounts[10]
    with reverts(INVALID_CALLBACK_CALL_ERR_MSG):
        contract.deleteMemberCallback(member_to_delete, {'from': not_voting_owner_addr}), 'Callback should fail if no voting was created'


def test_update_callback(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members

    voting_owner = members_accounts[0]
    new_name = 'new name'
    new_desc = 'new desc'
    new_membership_period = 999
    new_recovery_period = 111
    new_voting_time = 999
    new_turnout_threshold = 33
    contract.update(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': voting_owner})
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voting_owner, option=0)  # first callback is executed here

    organization_details = contract.getOrganizationDetails()
    assert new_name == organization_details[0]
    assert new_desc == organization_details[1]
    assert new_membership_period == organization_details[2]
    assert new_recovery_period == organization_details[3]
    assert new_voting_time == organization_details[4]
    assert new_turnout_threshold == organization_details[5]

    with reverts(INVALID_CALLBACK_CALL_ERR_MSG):
        voting_addr = contract.getVotings()[-1]
        contract.updateCallback(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': voting_addr}), 'callback action should be exectud only once'

    not_voting_owner_addr = accounts[10]
    with reverts(INVALID_CALLBACK_CALL_ERR_MSG):
        contract.updateCallback(new_name, new_desc, new_membership_period, new_recovery_period, new_voting_time, new_turnout_threshold, {'from': not_voting_owner_addr}), 'callback should fail if no voting was created'


def test_get_votings(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members
    init_votings_num = len(contract.getVotings())

    voting_owner = members_accounts[0]
    contract.invite(accounts[10], 100, 'test@mail.com', {'from': voting_owner})
    contract.update('test', 'test desc', 100, 50, 100, 55, {'from': voting_owner})
    member_to_delete = contract.getMember({'from': members_accounts[1]})
    contract.deleteMember(member_to_delete, {'from': voting_owner})

    new_votings_num = 3
    assert len(contract.getVotings()) == init_votings_num + new_votings_num, 'getVotings() should return all votings, including new created'

    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voting_owner, option=0)
    assert len(contract.getVotings()) == init_votings_num + new_votings_num, 'getVotings() should return all votings, no matter if finalized or active'


def test_delete_member_exists(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members
    
    voting_owner = members_accounts[0]
    member_to_delete_account = members_accounts[1]
    member_to_delete = contract.getMember({'from': member_to_delete_account})
    contract.deleteMember(member_to_delete, {'from': voting_owner})

    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voting_owner, option=1)
    assert contract.getMember({'from': member_to_delete_account}) != NULL_ADDRESS

    contract.deleteMember(member_to_delete, {'from': voting_owner})
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=voting_owner, option=0)
    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):   # no longer a member
        contract.getMember({'from': member_to_delete_account})


def test_delete_member_not_exists(dao_organization_contract):
    contract, contract_owner = dao_organization_contract

    member_addr_to_delete = accounts[3]
    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):   # not member for sure
        contract.getMember({'from': member_addr_to_delete})

    with reverts(MEMBER_NOT_EXIST):
        contract.deleteMember(member_addr_to_delete, {'from': contract_owner})


def test_add_member_success(dao_organization_contract):
    contract, _ = dao_organization_contract

    new_member_account = accounts[1]

    contract.invite(new_member_account, 3600, '')
    contract.addMember('name', 'surname', '3@mail.com', 'nick', {'from' : new_member_account})

    # check if member exists and by who was created
    member_contract_addr = contract.getMember({'from' : new_member_account})
    assert get_member_contract_owner(member_contract_addr) == new_member_account.address.lower()



def test_add_member_after_membership_expired(dao_organization_contract):
    contract, contract_owner = dao_organization_contract
    new_member_account = accounts[1]
    contract.invite(new_member_account, 100, '')

    dao_expire_memberships(contract)
    
    contract.addMember('', '', '', '', {'from': new_member_account})  # add new member invited before
    contract.invite(contract_owner, 100, '', {'from': new_member_account})  # re-invite member with expired membership 
    contract.addMember('', '', '', '', {'from': contract_owner})

    active_members = contract.getMembers()
    assert len(active_members) == 2, '1 members added again + 1 new members'


def test_add_member_deleted_member(dao_organization_contract):
    contract, contract_owner = dao_organization_contract

    assert len(contract.getMembers()) == 1, 'Only founder in organization'

    contract.invite(accounts[1], 100, '')
    contract.addMember('', '', '', '', {'from': accounts[1]})
    member_contract_addr = contract.getMember({'from': accounts[1]})
    
    contract.deleteMember(member_contract_addr, {'from': contract_owner})
    finalize_last_voting_with_option(dao_contract=contract, voting_contract_owner=contract_owner, option=0)
    active_members = contract.getMembers()
    assert len(active_members) == 1, 'One member was removed'
   
    contract.invite(accounts[1], 100, '')
    contract.addMember('', '', '', '', {'from': accounts[1]})
    active_members = contract.getMembers()
    assert len(active_members) == 2, 'User added once again'


def test_add_member_without_invitation(dao_organization_contract):
    contract, _ = dao_organization_contract

    with reverts(NO_INVITATION_ERR_MSG):
        contract.addMember('name', 'surname', '3@mail.com', 'nick')


def test_update_member(dao_organization_contract):
    contract, contract_owner = dao_organization_contract

    new_name = 'update name'
    new_surnname = 'updated surname'
    new_nick = 'updated nick'

    contract.updateMember(new_name, new_surnname, new_nick, {'from': contract_owner})
    member_contract = get_member_contract(contract.getMember({'from': contract_owner}))
    assert member_contract.getName() == new_name
    assert member_contract.getSurname() == new_surnname
    assert member_contract.getNickname() == new_nick    


def test_update_member_no_member_owner(dao_organization_contract):
    contract, _ = dao_organization_contract
    
    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.updateMember('', '', '', {'from': accounts[10]})


def test_leave_organization(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members
    
    member_to_leave = members_accounts[1]
    contract.leaveOrganization({'from': member_to_leave})

    assert len(contract.getMembers()) == len(members_accounts) - 1
    assert contract.checkIfMemberBelongsToOrganization({'from': member_to_leave}) is False


def test_leave_organization_not_member(dao_organization_contract):
    contract, _ = dao_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.leaveOrganization({'from': accounts[10]})


def test_renew_membership(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members
    member_account_addr = members_accounts[1]
    
    old_timestamp = contract.getMembershipStartTime({'from' : member_account_addr})
    contract.renewMembership({'from': member_account_addr})
    new_timestamp = contract.getMembershipStartTime({'from' : member_account_addr})

    assert new_timestamp > old_timestamp, 'The start time should be updated'


def test_renew_membership_not_member(dao_organization_contract):
    contract, _ = dao_organization_contract
    
    not_member_account_num = 2
    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.renewMembership({'from': accounts[not_member_account_num]})


def test_renew_membership_expired(dao_organization_contract):
    contract, contract_owner = dao_organization_contract
    
    dao_expire_memberships(contract)

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.renewMembership({'from': contract_owner})


def test_renew_membership_by_voting(dao_organization_contract_with_multiple_members):
    contract, members_accounts, _ = dao_organization_contract_with_multiple_members
    member_account_addr = members_accounts[1]
    
    old_timestamp = contract.getMembershipStartTime({'from' : member_account_addr})
    
    contract.invite(accounts[10], 100, 'test@mail.com', {'from': members_accounts[0]})
    voting_contract = get_voting_contract(contract.getVotings()[-1])
    voting_contract.vote(0, {'from': member_account_addr})
    
    new_timestamp = contract.getMembershipStartTime({'from' : member_account_addr})

    assert new_timestamp > old_timestamp, 'Membership should be renewed afer voting'


def test_get_membership_start_time(dao_organization_contract):
    contract, contract_owner = dao_organization_contract

    contract.invite(accounts[1], 100, 'test@mail.com')
    contract.addMember('', '', '', '', {'from': accounts[1]})

    assert contract.getMembershipStartTime({'from': contract_owner}) > 0
    assert contract.getMembershipStartTime({'from': accounts[1]}) > 0

    assert contract.getMembershipStartTime({'from': contract_owner}) < chain[-1].timestamp
    assert contract.getMembershipStartTime({'from': accounts[1]}) == chain[-1].timestamp


def test_get_membership_start_period_by_not_member(dao_organization_contract):
    contract, _ = dao_organization_contract

    with reverts(NOT_MEMBER_REQUEST_ERR_MSG):
        contract.getMembershipStartTime({'from': accounts[3]})
