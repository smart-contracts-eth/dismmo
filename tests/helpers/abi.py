member_get_owner_abi = [
    {
        'constant': 'true',
        'inputs': [],
        'name': 'getOwner',
        'outputs': [{
            'name': '',
            'type': 'address'
        }],
        'payable': 'false',
        'stateMutability': 'view',
        'type': 'function'
    }
]
