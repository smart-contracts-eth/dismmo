from brownie import Contract, accounts, network
from helpers.abi import member_get_owner_abi
import time
import json


MAX_ACCOUNT_NUMBER = 20
NULL_ADDRESS = '0x0000000000000000000000000000000000000000'


def get_member_contract_owner(member_address: str) -> str:
    contract = Contract.from_abi('Member', member_address, member_get_owner_abi)
    return contract.getOwner().lower()


def get_member_contract(member_address: str) -> str:
    contract_name = 'Member'
    with open(f"build/contracts/{contract_name}.json") as f:
        abi = json.load(f)["abi"]

    contract = Contract.from_abi(contract_name, member_address, abi)
    return contract


def get_voting_contract(member_address: str) -> str:
    contract_name = 'Voting'
    with open(f"build/contracts/{contract_name}.json") as f:
        abi = json.load(f)["abi"]

    contract = Contract.from_abi(contract_name, member_address, abi)
    return contract


def add_multiple_standard_members(organization_contract, accounts_list):
    for account_num in accounts_list:
        if account_num < MAX_ACCOUNT_NUMBER and account_num > 0:
            organization_contract.addMember('name', 'surname', f'{account_num}@mail.com', str(account_num), {'from' : accounts[account_num]})


def add_multiple_standard_members_with_invitation(organization_contract, accounts_list, admin_account_num=0):
    for account_num in accounts_list:
        if account_num < MAX_ACCOUNT_NUMBER and account_num >= 0:
            print(account_num)
            organization_contract.invite(accounts[account_num], 3600, '', {'from': accounts[admin_account_num]})
            organization_contract.addMember('name', 'surname', f'{account_num}@mail.com', str(account_num), {'from' : accounts[account_num]})


def add_member_with_role(organization_contract, account, role=2, admin_account_num=0):
    organization_contract.invite(account, 3600, '', {'from': accounts[admin_account_num]})
    organization_contract.addMember('name', 'surname', 'test@mail.com', 'test nick', {'from' : account})

    if role == 1:
        organization_contract.setMemberRole(account, role)


def expire_memberships(organization_contract, membership_period_in_seconds = 4):
    ''' this method should not be used for a longer operations, at least without changing deafault membership period '''
    organization_details = organization_contract.getOrganizationDetails()
    organization_name = organization_details[0]
    organization_desc = organization_details[1]
    new_membership_period = membership_period_in_seconds
    new_recovery_period = membership_period_in_seconds - 1

    organization_contract.update(organization_name, organization_desc, new_membership_period, new_recovery_period)
    time.sleep(membership_period_in_seconds + 1)

    # add a new block to have current time(block.timestamp). It's not the problem in real environment, because 
    # new blocks are being created frequently and organization membership is usually longer then a few seconds.
    network.chain.mine()


def dao_expire_memberships(organization_contract, membership_period_in_seconds = 4):
    ''' this method should not be used for a longer operations, at least without changing deafault membership period '''
    organization_details = organization_contract.getOrganizationDetails()
    organization_name = organization_details[0]
    organization_desc = organization_details[1]
    voting_time = organization_details[4]
    turnout_threshold = organization_details[5]
    new_membership_period = membership_period_in_seconds
    new_recovery_period = membership_period_in_seconds - 1

    organization_contract.update(organization_name, organization_desc, new_membership_period, new_recovery_period, voting_time, turnout_threshold)
    if len(organization_contract.getMembers()) > 1:
        member_contract_owner = get_member_contract_owner(organization_contract.getMember())
        finalize_last_voting_with_option(organization_contract, member_contract_owner, 0)
    time.sleep(membership_period_in_seconds + 1)

    # add a new block to have current time(block.timestamp). It's not the problem in real environment, because 
    # new blocks are being created frequently and organization membership is usually longer then a few seconds.
    network.chain.mine()


def go_to_recovery_period(organization_contract):
    ''' simulate that member is in recovery period '''
    organization_details = organization_contract.getOrganizationDetails()
    organization_name = organization_details[0]
    organization_desc = organization_details[1]
    membership_period = organization_details[2]
    new_recovery_period = membership_period - 1

    organization_contract.update(organization_name, organization_desc, membership_period, new_recovery_period)


def finalize_last_voting_with_option(dao_contract, voting_contract_owner, option=0):
    voting_time = dao_contract.getOrganizationDetails()[4]
    voting = dao_contract.getVotings()[-1]
    voting_contract = get_voting_contract(voting)

    voting_contract.vote(option, {'from': voting_contract_owner})
    time.sleep(voting_time)
    network.chain.mine()
    voting_contract.finalizeVoting({'from': voting_contract_owner})
