# Projekt rozproszonej aplikacji do zarządzania członkostwem w organizacjach w Bloxberg.

## Strona wydziałowa ZPI   
https://wit.pwr.edu.pl/studenci/organizacja-toku-studiow/zpi  

## Dokumentacja ZPI 
Oficjalna dokumentacja projektu do oddania  
https://docs.google.com/document/d/1Cd_-DlNALOC1dMHFDNT4HzV1eLuWDQiE/edit?usp=sharing&ouid=102934766892309902590&rtpof=true&sd=true

## Jira
Do zarządzania zadaniami w zespole  
https://dismmo.atlassian.net/jira/your-work  
Plan > Jira  

## Confluence
Wiki projektu  
https://gitlab.com/pwr-zpi-gr-7/dismmo/-/wikis/-/confluence  
Plan > Confluence  
