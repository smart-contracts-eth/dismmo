// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;


import "./Member.sol";


contract Organization {

    // Members that have not renewed theirs membership are still active, but contract does not consider them as
    // current organization members - it can calculate if last renewed time is longer than membership period.
    // Such thing has to be done, because backend cannot spend gas interacting with the contract.
    // So an organization can have:
    // - active members (they renew membership)
    // - non-active members (they do not renew membership)
    // - ousted members (they are not members now)

    struct Membership {
        uint membershipStartTime;
        bool ousted;
    }

    struct Invitation {
        address invitee;
        address inviter;
        bool active;
        uint invitationStart;
        uint invitationDuration;
    }

    string name;
    string description;
    uint membershipPeriod;
    uint recoveryPeriod;

    uint membersCount = 0;
    
    Member[] internal members; 
    mapping(Member => Membership) internal membershipDetails;

    mapping(address => Invitation) internal invitees;


    address constant NULL_ADDRESS = address(0x0);
    string constant internal INVALID_TIME_ERR_MSG = 'Time should be greater than 0';
    string constant internal RECOVERY_PERIOD_LONGER_THAN_MEMBERHIP_PERIOD_ERR_MSG = 'Recovery time longer than membership period';
    string constant internal NOT_MEMBER_REQUEST_ERR_MSG = 'Requester is not a member';
    string constant internal MEMBER_ALREADY_EXISTS = 'Member already exists';
    string constant internal MEMBER_NOT_EXIST = 'Member does not exist';
    string constant internal NO_INVITATION_ERR_MSG = 'No active invitation for this address';
    string constant internal INVITATION_INACTIVE_ERR_MSG = 'Invitation expired';
    string constant internal NOT_INVITER_REQUEST_ERR_MSG = 'No inviter can not cancel invitation';

    event InvitationSent(address indexed invitee, address inviter, string invitedPersonEmail);


    constructor(string memory p_name, string memory p_description, uint p_membershipPeriod, uint p_recoveryPeriod) {
        _update(p_name, p_description, p_membershipPeriod, p_recoveryPeriod);
    }

    function _getOrganizationType() public pure returns(string memory) {
        return "ORGANIZATION";
    }

    function _update(
        string memory p_name,
        string memory p_description,
        uint p_membershipPeriod,
        uint p_recoveryPeriod
    ) 
        internal
    {
        if (p_membershipPeriod < p_recoveryPeriod) {
            revert(RECOVERY_PERIOD_LONGER_THAN_MEMBERHIP_PERIOD_ERR_MSG);
        }
                        
        name = p_name;
        description = p_description;
        _setMembershipPeriod(p_membershipPeriod);
        _setRecoveryPeriod(p_recoveryPeriod);
    }

    function _invite(address invitee, uint p_invitationDuration, string memory invitedPersonEmail) internal {
        if (_checkIfMemberBelongsToOrganization(invitee)) {
            revert(MEMBER_ALREADY_EXISTS);
        }

        Invitation memory invitation;
        invitation.invitee = invitee;
        invitation.inviter = tx.origin;
        invitation.active = true;
        invitation.invitationStart = block.timestamp;
        invitation.invitationDuration = p_invitationDuration;

        invitees[invitee] = invitation;

        emit InvitationSent(invitee, tx.origin, invitedPersonEmail);
    }

    function _cancelInvitation(address invitee) internal {
        if (tx.origin != invitees[invitee].inviter) {
            revert(NOT_INVITER_REQUEST_ERR_MSG);
        }
        if (!invitees[invitee].active) {
            revert(INVITATION_INACTIVE_ERR_MSG);
        }
        invitees[invitee].active = false;
    }
    
    function _addMember(
        string calldata p_name,
        string calldata p_surname,
        string calldata p_email,
        string calldata p_nickname,
        Member.Role p_role
    ) 
        internal 
    {
        if (!_checkIfInvited(tx.origin)) {
            revert(NO_INVITATION_ERR_MSG);
        }

        if (_checkIfMemberBelongsToOrganization(tx.origin)) {
            revert(MEMBER_ALREADY_EXISTS);
        }

        Member newMember = new Member(p_name, p_surname, p_email, p_nickname, p_role);
        _addMember(newMember);
        delete invitees[tx.origin];
    }

    function _setMemberEmail(string calldata p_email) internal {
        address addr = _getMember();
        if (addr != NULL_ADDRESS) {
            Member m = Member(addr);
            m.setEmail(p_email);
        }
    }

    function _updateMember(string calldata p_name, string calldata p_surname, string calldata p_nickname) internal {
        address addr = _getMember();
        if (addr != NULL_ADDRESS) {
            Member m = Member(addr);
            m.setName(p_name);
            m.setSurname(p_surname);
            m.setNickname(p_nickname);
        }
    }

    function _deleteMember(address m) internal {
        Member mem = Member(m);
        for (uint i  =0; i < membersCount; i++) {
            if (members[i] == mem) {
                Member currentMember = members[i];
                membershipDetails[currentMember].ousted = true;
                return;
            }
        }
        revert(MEMBER_NOT_EXIST);
    }

    function _renewMembership() internal {
        address memberAddress = _getMember();

        if (memberAddress == NULL_ADDRESS) {
            revert(NOT_MEMBER_REQUEST_ERR_MSG);
        }

        Member member = Member(memberAddress);
        for (uint i = 0; i < membersCount; i++) {
           if (members[i] == member) {
                Member currentMember = members[i];
                membershipDetails[currentMember].membershipStartTime = block.timestamp;
           }
        }
    }

    function _getMembers() internal view returns(Member[] memory) {
        uint32 activeMembersCount = _getNumberOfActiveMembers();
        Member[] memory membersToNotify = new Member[](activeMembersCount);

        for (uint i = 0; i < membersCount; i++) {
            Member currentMember = members[i];
            if (!_checkIfMembershipExpired(currentMember)) {
                membersToNotify[--activeMembersCount] = members[i];
            }
        }
        return membersToNotify; 
    }

    function _getMembersAddresses() internal view returns(address[] memory) {
        uint32 activeMembersCount = _getNumberOfActiveMembers();
        address[] memory membersAddresses = new address[](activeMembersCount);

        for (uint i = 0; i < membersCount; i++) {
            Member currentMember = members[i];
            if (!_checkIfMembershipExpired(currentMember)) {
                membersAddresses[--activeMembersCount] = members[i].getOwner();
            }
        }
        return membersAddresses; 
    }

    function _getMember() internal view returns(address) {
        for (uint i = 0; i < membersCount; i++) {
            Member currentMember = members[i];
            if (currentMember.getOwner() == tx.origin && !_checkIfMembershipExpired(currentMember)) {
                return address(currentMember);
            }
        }
        return NULL_ADDRESS;
    }

    function _getMemberByWallet(address wallet) internal view returns(address) {
        for (uint i = 0; i < membersCount; i++) {
            Member currentMember = members[i];
            if (!_checkIfMembershipExpired(currentMember) && currentMember.getOwner() == wallet) {
                return address(currentMember);
            }
        }
        return NULL_ADDRESS;
    }

    function _checkIfMembershipExpired(Member member) internal view returns(bool) {
        for (uint i = 0; i < membersCount; i++) {
            if (members[i] == member) {
                return membershipDetails[member].ousted || 
                        (membershipDetails[member].membershipStartTime + membershipPeriod <= block.timestamp);
            }
        }
        revert(MEMBER_NOT_EXIST);
    }

    function _getAllMembersEmails() internal view returns (string[] memory) {
        string[] memory allMembersEmails = new string[](membersCount);

        for (uint i = 0; i < membersCount; i++) {
            allMembersEmails[i] = members[i].getEmail();
        }
        return allMembersEmails;
    }

    function _getExpiringMembershipsEmails() internal view returns (string[] memory) {
        uint currentTime = block.timestamp;
        uint expiringCount = 0;
        string[] memory tempEmails = new string[](membersCount);

        for (uint i = 0; i < membersCount; i++) {
            Membership memory currentMembership = membershipDetails[members[i]];
            uint timeLeft = currentMembership.membershipStartTime + membershipPeriod - currentTime;

            if (timeLeft <= recoveryPeriod && timeLeft >= recoveryPeriod - 86400 && !currentMembership.ousted) {
                tempEmails[expiringCount] = members[i].getEmail();
                expiringCount++;
            }
        }

        string[] memory expiringMembersEmails = new string[](expiringCount);

        for (uint i = 0; i < expiringCount; i++) {
            expiringMembersEmails[i] = tempEmails[i];
        }

        return expiringMembersEmails;
    }

    function _getExpiringMembershipsEmailsOneDayBeforeExpiring() internal view returns (string[] memory) {
        uint currentTime = block.timestamp;
        uint expiringCount = 0;
        string[] memory tempEmails = new string[](membersCount);

        for (uint i = 0; i < membersCount; i++) {
            Membership memory currentMembership = membershipDetails[members[i]];
            uint timeLeft = currentMembership.membershipStartTime + membershipPeriod - currentTime;

            if (timeLeft <= 86400 && timeLeft >= 0 && !currentMembership.ousted) {
                tempEmails[expiringCount] = members[i].getEmail();
                expiringCount++;
            }
        }

        string[] memory expiringMembersEmails = new string[](expiringCount);

        for (uint i = 0; i < expiringCount; i++) {
            expiringMembersEmails[i] = tempEmails[i];
        }

        return expiringMembersEmails;
    }

    function _checkIfMemberBelongsToOrganization(address walletAddress) internal view returns(bool) {
        
        for (uint i = 0; i< membersCount; i++) {
            if (!_checkIfMembershipExpired(members[i]) && members[i].getOwner() == walletAddress) {
                return true;
            }
        }
        return false;
    }

    function _checkIfInvited(address person) internal view returns(bool) {
        return invitees[person].active &&
                block.timestamp >= invitees[person].invitationStart &&
                block.timestamp < invitees[person].invitationStart + invitees[person].invitationDuration;
    }

    function _getMembershipStartTime() internal view returns(uint) {
        address memberAddress = _getMember();

        if (memberAddress == NULL_ADDRESS) {
            revert(NOT_MEMBER_REQUEST_ERR_MSG);
        }

        Member member = Member(memberAddress);
        for (uint i = 0; i < membersCount; i++) {
           if (members[i] == member) {
                Member currentMember = members[i];
                return membershipDetails[currentMember].membershipStartTime;
           }
        }
        return 0;
    }

    function _addMember(Member newNember) internal {
        // replace member if existed before and was added again
        for (uint i = 0; i < membersCount; ++i) {

            if (members[i].getOwner() == newNember.getOwner()) {
                members[i] = newNember;
                membershipDetails[members[i]] = Membership(block.timestamp, false);
                
                return;
            }
        }

        members.push(newNember);
        Membership memory membership = Membership(block.timestamp, false);
        membershipDetails[members[membersCount]] = membership;

        membersCount++;
    }

    function _getNumberOfActiveMembers() internal view returns(uint32) {
        uint32 activeMembersCount = 0;

        for (uint i = 0; i < membersCount; i++) {
            if (!_checkIfMembershipExpired(members[i])) {
                activeMembersCount++;
            }
        }

        return activeMembersCount;
    }

    function compareStrings(string memory a, string memory b) internal pure returns(bool) {
        return (keccak256(abi.encodePacked((a))) == keccak256(abi.encodePacked((b))));
    }

    function _setMembershipPeriod(uint p_membershipPeriod) private {
        if (p_membershipPeriod <= 0) {
            revert(INVALID_TIME_ERR_MSG);
        }

        uint currentTime = block.timestamp;
        for (uint i = 0; i < membersCount; i++) {
            Member currentMember = members[i];
            
            // mark members with expired membership to not re-activate them by extending time
            if (membershipDetails[currentMember].membershipStartTime + membershipPeriod < currentTime) {
                membershipDetails[currentMember].ousted = true;
            } else {
                // when new membership time is shorted, conpensate it for existing members 
                // but with a limit to current time
                if (p_membershipPeriod < membershipPeriod) {
                    uint newMembershipStartTime = 
                        membershipDetails[currentMember].membershipStartTime + (membershipPeriod - p_membershipPeriod);
                    
                    if (newMembershipStartTime > currentTime) {
                        membershipDetails[currentMember].membershipStartTime = currentTime;
                    } else {
                        membershipDetails[currentMember].membershipStartTime = newMembershipStartTime;
                    }
                }
            }
        }

        membershipPeriod = p_membershipPeriod;
    }

    function _setRecoveryPeriod(uint p_recoveryPeriod) private {
        if (p_recoveryPeriod <= 0) {
            revert(INVALID_TIME_ERR_MSG);
        }

        recoveryPeriod = p_recoveryPeriod;
    }
}
