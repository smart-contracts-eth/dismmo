// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./lib/StringConversions.sol";

contract Voting
{
    struct Voter {        
        bool voted;  // if true, that person already voted
        uint vote;
        address addr;
        uint weight;
    }

    struct Option {
        string name;
        uint voteCount; // number of accumulated votes
        bytes payload;
    }

    struct WinningOptions {
        mapping (uint => uint) options;
        uint numOptions;
        bool valid;
    }

    string constant TOO_SHORT_VOTING_TIME_ERR = "Too short voting time";
    string constant OPTIONS_NUMBER_NOT_EQUAL_TO_PAYLOAD_ERR = "Options number is different than payloads number";
    string constant INVALID_TURNOUT_THRESHOLD_ERR_MSG = "Turnout threshold should be integer value from 0 to 100";
    string constant VOTING_TIME_EXPIRED_ERR_MSG = "Voting period expired";
    string constant INVALID_OPTION_ERR = "Invalid option";
    string constant VOTING_NOT_STARTED_ERR_MSG = "Voting not started yet";
    string constant VOTING_NOT_FINISHED_ERR_MSG = "Voting not finished yet";
    string constant NOT_VOTING_OWNER_ERR = "Not voting owner";
    string constant VOTING_ALREADY_STARTED_ERR = "Voting already started";
    string constant USER_ALREADY_VOTED_ERR_MSG = "User already voted";
    string constant USER_NOT_ALLOWED_FOR_VOTING_ERR_MSG = "User cannot vote in this voting - created before user was added";
    uint constant MINIMUM_VOTING_TIME = /*86400*/ 1; // set to 1s for easier testing; default should be one day

    string name;
    uint votingStart;
    uint votingTime;
    bool isOpened;
    bool isStarted;
    bool isFinalized;
    address contractToCall;
    address public chairperson;    
    mapping(uint => Voter) public voters;
    uint public numVoters;    
    uint voterTurnoutThreshold;
    bytes callbackWhenVoting;

    mapping (uint => Option) options;
    uint numOptions;
    uint numCastedVotes;
    WinningOptions winningOptions;

    constructor(
        string memory p_name, 
        string[] memory optionNames, 
        address p_contractToCall, 
        bytes[] memory p_payloads,
        uint p_voterTurnoutThreshold,
        bytes memory p_callbackWhenVoting
    ) 
    {
        if (p_voterTurnoutThreshold < 0 || p_voterTurnoutThreshold > 100) {
            revert(INVALID_TURNOUT_THRESHOLD_ERR_MSG);
        }

        if (optionNames.length != p_payloads.length) {
            revert(OPTIONS_NUMBER_NOT_EQUAL_TO_PAYLOAD_ERR);
        }

        chairperson = tx.origin;
        name = p_name;
        numOptions = optionNames.length;
        isOpened=false;
        isStarted = false;
        isFinalized = false;
        numVoters = 0;
        numCastedVotes = 0;
        numOptions = optionNames.length;
        contractToCall = p_contractToCall;
        voterTurnoutThreshold = p_voterTurnoutThreshold;
        callbackWhenVoting = p_callbackWhenVoting;

        for (uint i = 0; i < optionNames.length; i++) {
            options[i].name = optionNames[i];
            options[i].voteCount = 0;
            options[i].payload = p_payloads[i];
        }
    }

    function startVoting(address[] memory p_voters, uint p_votingTime) public {
        if (tx.origin != chairperson) {
            revert(NOT_VOTING_OWNER_ERR);
        }

        if (isStarted) {
            revert(VOTING_ALREADY_STARTED_ERR);
        }

        if(p_votingTime < MINIMUM_VOTING_TIME){
            revert(TOO_SHORT_VOTING_TIME_ERR);
        }

        numVoters = p_voters.length;
        for(uint i = 0; i < numVoters; i++){
            Voter storage v = voters[i];
            v.addr = p_voters[i];
            v.voted = false;
            v.weight = 1;
        }

        votingStart = block.timestamp;
        votingTime = p_votingTime;
        isOpened=true;
        isStarted = true;
    }

    function vote(uint p_option) public {
        if (!isStarted) {
            revert(VOTING_NOT_STARTED_ERR_MSG);
        }

        if (checkIfVotingTimeEnded()) {
            revert(VOTING_TIME_EXPIRED_ERR_MSG);
        }

        if (!checkIfVotedOptionExists(p_option)) {
            revert(INVALID_OPTION_ERR);
        }
        
        for (uint i = 0; i <= numVoters; i++) {
            if (voters[i].addr == tx.origin) {
                Voter storage sender = voters[i];
                
                if (sender.voted == true) {
                    revert(USER_ALREADY_VOTED_ERR_MSG);
                }
                
                sender.voted = true;
                sender.vote = p_option;
                options[p_option].voteCount += sender.weight;
                numCastedVotes += 1;

                (bool success, bytes memory returnData) = contractToCall.call(callbackWhenVoting);
                if (!success) {
                    propagateRevert(returnData);
                }

                return;
            }
        }
        revert(USER_NOT_ALLOWED_FOR_VOTING_ERR_MSG);
    }

    function finalizeVoting() public {
        if (tx.origin != chairperson) {            
            revert(NOT_VOTING_OWNER_ERR);
        }
        if (!isStarted) {
            revert(VOTING_NOT_STARTED_ERR_MSG);
        }
        if (!checkIfVotingTimeEnded()) {
            revert(VOTING_NOT_FINISHED_ERR_MSG);
        }

        getWinningOptions();

        if (winningOptions.numOptions == 1) {

            if (numCastedVotes*100 >= voterTurnoutThreshold*numVoters) {
                (bool success, bytes memory returnData) = contractToCall.call(options[winningOptions.options[0]].payload);
            }
        }
        
        isOpened=false;
        isFinalized = true;
    }

    function checkIfVotingTimeEnded() public view returns(bool) {
        return block.timestamp > votingStart + votingTime;
    }

    function checkIfVotedOptionExists(uint p_option) public view returns(bool) {
        return p_option < numOptions;
    }
    
    function hasUserVoted() public view returns (bool result) {
        for (uint i = 0; i <= numVoters; i++) {
            if (voters[i].addr == tx.origin) {
                return voters[i].voted;
            }
        }
        return false;
    }

    function getVotingResult() public view returns (Option[] memory) {
        if (checkIfVotingTimeEnded() && isFinalized) {
            Option[] memory lOptions = new Option[](numOptions);
            for (uint i = 0; i < numOptions; i++) {
                Option storage lOption = options[i];
                lOptions[i] = lOption;
            }
            return lOptions;
        } else {
            revert(VOTING_NOT_FINISHED_ERR_MSG);
        }
    }
    
    function getVotingName() public view returns (string memory) {               
        return name;        
    }

    function getVotingDetails() public view returns(string memory) {
        string memory s_hasUserVoted = hasUserVoted() ? "true" : "false";
        string memory s_checkIfVotingTimeEnded = checkIfVotingTimeEnded() ? "true" : "false";
        string memory s_isOpened = isOpened ? "true" : "false";

        return string.concat('{"name" : ', name, ', ',
                                '"chairperson" : "', StringConversions.toString(chairperson), '", ',
                                '"hasUserVoted" : "', s_hasUserVoted, '", ',
                                '"votingStart" : "', StringConversions.toString(votingStart), '", ',
                                '"votingTime" : "', StringConversions.toString(votingTime), '", ',
                                '"isOpened" : "', s_isOpened, '", ',
                                '"timeEnded" : "', s_checkIfVotingTimeEnded, '"',
                            "}"
                            );
    }

    function checkIfVotingIsFinalized() public view returns (bool) {
        return isFinalized;
    }

    function getMinimumVotingTime() public pure returns(uint) {
        return MINIMUM_VOTING_TIME;
    }

    function getWinningOptions() private {
        winningOptions.numOptions = 0;
        uint maxNrOfVotesPerOption = 0;

        for (uint i = 0; i < numOptions; i++) {
            if (maxNrOfVotesPerOption == options[i].voteCount) {
                winningOptions.options[winningOptions.numOptions] = i;
                winningOptions.numOptions++;
            } else if (maxNrOfVotesPerOption < options[i].voteCount) {
                maxNrOfVotesPerOption = options[i].voteCount;
                winningOptions.options[0] = i;
                winningOptions.numOptions = 1;
            }
        }
    }

    function getNumberOfVotersWhoVoted() private view returns (uint) {
        uint numberOfVotersWhoVoted = 0;
        for (uint i = 0; i <= numVoters; i++) {
            if (voters[i].voted) {
                numberOfVotersWhoVoted++;
            }
        }

        return numberOfVotersWhoVoted;
    }

    function propagateRevert(bytes memory returnData) private pure {
        if (returnData.length > 68) { // 68 bytes - enough data to safetly extract string messgae
            assembly {
                returnData := add(returnData, 0x04) // strip function signature
            }
            revert(abi.decode(returnData, (string)));
        } else {
            revert("Reverted with unknown reason");
        }
    }
}
