// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./Organization.sol";
import "./IVotingsManager.sol";
import "./VotingsManager.sol";

contract DecentralizedAutonomousOrganization is Organization {
    struct OrganizationDetails {
        string name;
        string description;
        uint membershipPeriod;
        uint recoveryPeriod;
        uint votingTime;
        uint voterTurnoutThreshold;
        string organizationType;
    }

    event VotingCreated(
        string name,
        string[] emails
    );

    uint private votingTime;
    uint private voterTurnoutThreshold;
    IVotingsManager private votingsManager;

    string constant INVALID_VOTING_TIME_ERR_MSG = "Voting time is too low";
    string constant INVALID_TURNOUT_THRESHOLD_ERR_MSG = "Turnout threshold should be percentage value(0-100)";
    string constant INVALID_CALLBACK_CALL_ERR_MSG = "Invalid callback call - voting not found";

    constructor(
        string memory p_name,
        string memory p_description,
        uint p_membershipPeriod,
        uint p_recoveryPeriod,
        uint p_votingTime,
        uint p_voterTurnoutThreshold
    )
        Organization(p_name, p_description, p_membershipPeriod, p_recoveryPeriod)
    {
        Member founder = new Member("", "", "", "", Member.Role.STANDARD);
        _addMember(founder);

        setVotingTime(p_votingTime);
        setVoterTurnoutThreshold(p_voterTurnoutThreshold);

        votingsManager = new VotingsManager(votingTime, voterTurnoutThreshold);
    }

    function update(
        string calldata p_name,
        string calldata p_description,
        uint p_membershipPeriod,
        uint p_recoveryPeriod,
        uint p_votingTime,
        uint p_voterTurnoutThreshold
    ) 
        public 
    {
        memberOperationGuard();

        if (checkIfProceedWithoutVoting()) {
            setVotingTime(p_votingTime);
            setVoterTurnoutThreshold(p_voterTurnoutThreshold);
            votingsManager.updateVotingParameters(p_votingTime, p_voterTurnoutThreshold);

            _update(p_name, p_description, p_membershipPeriod, p_recoveryPeriod);
        } else {
            votingsManager.createVotingForOrganizationDetailsChange(
                _getMembersAddresses(),
                p_name,
                p_description,
                p_membershipPeriod,
                p_recoveryPeriod,
                p_votingTime,
                p_voterTurnoutThreshold
            );
            emitVotingCreatedEvent("Update organization details");
        }
    }

    function invite(address invitee, uint p_invitationDuration, string memory invitedPersonEmail) public {
        memberOperationGuard();

        if (checkIfProceedWithoutVoting()) {
            _invite(invitee, p_invitationDuration, invitedPersonEmail);
        } else {
            votingsManager.createVotingForMemberInvite(
                _getMembersAddresses(), 
                invitee, 
                p_invitationDuration, 
                invitedPersonEmail
            );
            emitVotingCreatedEvent("Invite member to organization");
        }
    }

    function addMember(
        string calldata p_name,
        string calldata p_surname,
        string calldata p_email,
        string calldata p_nickname
    ) 
        public 
    {
        _addMember(p_name, p_surname, p_email, p_nickname, Member.Role.STANDARD);
    }

    function setMemberEmail(string calldata p_email) public {
        memberOperationGuard();
        _setMemberEmail(p_email);
    }

    function updateMember(
        string calldata p_name,
        string calldata p_surname,
        string calldata p_nickname
    ) 
        public 
    {
        memberOperationGuard();
        _updateMember(p_name, p_surname, p_nickname);
    }

    function deleteMember(address memberContractAddr) public {
        memberOperationGuard();

        if (checkIfProceedWithoutVoting()) {
            _deleteMember(memberContractAddr);
        } else {
            votingsManager.createVotingForMemberDelete(_getMembersAddresses(), memberContractAddr);
            emitVotingCreatedEvent("Delete member from organization");
        }
    }

    function leaveOrganization() public {
        memberOperationGuard();

        address member = _getMemberByWallet(tx.origin);
        _deleteMember(member);
    }

    function renewMembership() public {
        memberOperationGuard();

        _renewMembership();
    }

    function updateCallback(
        string calldata p_name,
        string calldata p_description,
        uint p_membershipPeriod,
        uint p_recoveryPeriod,
        uint p_votingTime,
        uint p_voterTurnoutThreshold
    ) 
        public 
    {
        if (!votingsManager.checkIfVotingIsAuthorisedToCallback(msg.sender)) {
            revert(INVALID_CALLBACK_CALL_ERR_MSG);
        }

        setVotingTime(p_votingTime);
        setVoterTurnoutThreshold(p_voterTurnoutThreshold);
        votingsManager.updateVotingParameters(p_votingTime, p_voterTurnoutThreshold);
        
        _update(p_name, p_description, p_membershipPeriod, p_recoveryPeriod);
    }

    function deleteMemberCallback(address m) public {
        if (!votingsManager.checkIfVotingIsAuthorisedToCallback(msg.sender)) {
            revert(INVALID_CALLBACK_CALL_ERR_MSG);
        }

        _deleteMember(m);
    }

    function inviteCallback(address invitee, uint p_invitationDuration, string memory invitedPersonEmail) public {
        if (!votingsManager.checkIfVotingIsAuthorisedToCallback(msg.sender)) {
            revert(INVALID_CALLBACK_CALL_ERR_MSG);
        }

        _invite(invitee, p_invitationDuration, invitedPersonEmail);
    }

    function getMembers() public view returns (Member[] memory) {
        memberOperationGuard();
        return _getMembers();
    }

    function getMember() public view returns (address) {
        memberOperationGuard();
        return _getMember();
    }

    function checkIfMembershipExpired(Member member) public view returns (bool) {
        memberOperationGuard();
        return _checkIfMembershipExpired(member);
    }

    function getAllMembersEmails() internal view returns (string[] memory) {
        return _getAllMembersEmails();
    }

    function getExpiringMembershipsEmails() public view returns (string[] memory) {
        return _getExpiringMembershipsEmails();
    }

    function getExpiringMembershipsEmailsOneDayBeforeExpiring() public view returns (string[] memory)
    {
        return _getExpiringMembershipsEmailsOneDayBeforeExpiring();
    }

    function checkIfMemberBelongsToOrganization() public view returns (bool) {
        return _checkIfMemberBelongsToOrganization(tx.origin);
    }

    function getOrganizationDetails() public view returns (OrganizationDetails memory) {
        memberOperationGuard();

        return OrganizationDetails({
                name: name,
                description: description,
                membershipPeriod: membershipPeriod,
                recoveryPeriod: recoveryPeriod,
                votingTime: votingTime,
                voterTurnoutThreshold: voterTurnoutThreshold,
                organizationType: getOrganizationType()
            });
    }

    function getVotings() public view returns (Voting[] memory) {
        memberOperationGuard();

        return votingsManager.getVotings();
    }

    function checkIfInvited(address person) public view returns (bool) {
        return _checkIfInvited(person);
    }

    function getMembershipStartTime() public view returns (uint) {
        memberOperationGuard();
        return _getMembershipStartTime();
    }

    // dev/null for voting with no effect
    function devNull() public pure {
    }

    function getOrganizationType() public pure returns (string memory) {
        return "DECENTRALIZED_AUTONOMOUS_ORGANIZATION";
    }

    function setVotingTime(uint p_votingTime) private {
        if (p_votingTime <= 0) {
            revert(INVALID_VOTING_TIME_ERR_MSG);
        }

        votingTime = p_votingTime;
    }

    function setVoterTurnoutThreshold(uint p_voterTurnoutThreshold) private {
        if (p_voterTurnoutThreshold <= 0 || p_voterTurnoutThreshold > 100) {
            revert(INVALID_TURNOUT_THRESHOLD_ERR_MSG);
        }

        voterTurnoutThreshold = p_voterTurnoutThreshold;
    }
    
    function memberOperationGuard() private view {
        if (!_checkIfMemberBelongsToOrganization(tx.origin)) {
            revert(NOT_MEMBER_REQUEST_ERR_MSG);
        }
    }

    function checkIfProceedWithoutVoting() private view returns (bool) {
        return _getMembers().length <= 1;
    }

    function emitVotingCreatedEvent(string memory votingName) private {
        string[] memory emails = getAllMembersEmails();
        emit VotingCreated(votingName, emails);
    }
}
