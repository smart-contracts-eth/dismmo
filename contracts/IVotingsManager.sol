// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./Voting.sol";

interface IVotingsManager {

    function createVotingForMemberDelete(address[] memory p_voters, address member) external;


    function createVotingForMemberInvite(
        address[] memory p_voters,
        address invitee,
        uint p_invitationDuration,
        string memory invitedPersonEmail
    )
        external 
    ;

    function createVotingForOrganizationDetailsChange(
        address[] memory p_voters,
        string memory p_name,
        string memory p_description,
        uint p_membershipPeriod,
        uint p_recoveryPeriod,
        uint p_votingTime,
        uint p_voterTurnoutThreshold
    )
        external
    ;

    function updateVotingParameters(uint p_votingTime, uint p_voterTurnoutThreshold)
        external
    ;

    function getVotings() external view returns (Voting[] memory);

    function checkIfVotingIsAuthorisedToCallback(address sender) external view returns (bool);
}
