// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract OrganizationProxy {
    address private organizationAddress;
    address private proxyOwner = tx.origin;

    fallback(bytes calldata) external payable returns (bytes memory){
        (bool success, bytes memory returnData) = organizationAddress.call{value: msg.value}(msg.data);
        
        if (!success) {
            propagateRevert(returnData);
        }
        return returnData;
    }

    function updateOrganizationAddress(address newAddress) public {
        if (tx.origin == proxyOwner) {
            organizationAddress = newAddress;
        } else {
            revert("Only owner can change organization address");
        }
    }

    function propagateRevert(bytes memory returnData) private pure {
        if (returnData.length > 68) { // 68 bytes - enough data to safetly extract string messgae
            assembly {
                returnData := add(returnData, 0x04) // strip function signature
            }
            revert(abi.decode(returnData, (string)));
        } else {
            revert("Reverted with unknown reason");
        }
    }
}
