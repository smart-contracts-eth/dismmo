// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./Voting.sol";
import "./IVotingsManager.sol";
import "./lib/StringConversions.sol";

contract VotingsManager is IVotingsManager{

    string constant private NOT_OWNER_ERR_MSG = "Not owner request";

    uint private votingTime;
    uint private voterTurnoutThreshold;
    mapping(uint => Voting) private votings;
    uint private numberVotings = 0;
    address private ownerAddress;

    string[] private binaryVotingAnswersTemplate = ["yes", "no"];
    
    constructor(uint p_votingTime, uint p_voterTurnoutThreshold) {
        votingTime = p_votingTime;
        voterTurnoutThreshold = p_voterTurnoutThreshold;
        ownerAddress = msg.sender;
    }

    function createVotingForMemberDelete(address[] memory p_voters, address memberContractAddr) public override {
        validateAccessRights();

        string memory textForVoting = string.concat('{"title" : ', '"Delete member", ',
                                                        '"parameters": {',
                                                            '"address" : "', StringConversions.toString(memberContractAddr), '"',
                                                        "}",
                                                    "}"
                                                    );

        bytes[] memory callbackFunctions = new bytes[](2);

        callbackFunctions[0] = abi.encodeWithSignature("deleteMemberCallback(address)", memberContractAddr);
        callbackFunctions[1] = abi.encodeWithSignature("devnull()");
        
        bytes memory callbackWhenVoting = abi.encodeWithSignature("renewMembership()");

        votings[numberVotings] = new Voting(
            textForVoting,
            binaryVotingAnswersTemplate,
            address(ownerAddress),
            callbackFunctions,
            voterTurnoutThreshold,
            callbackWhenVoting
        );

        votings[numberVotings].startVoting(p_voters, votingTime);

        numberVotings++;
    }

    function createVotingForMemberInvite(
        address[] memory p_voters,
        address invitee,
        uint p_invitationDuration,
        string memory invitedPersonEmail
    )
        public
        override
    {
        validateAccessRights();

        string memory textForVoting = string.concat('{"title" : ', '"Invite member", ',
                                                        '"parameters": {',
                                                            '"address" : "', StringConversions.toString(invitee), '", ',
                                                            '"invitationDuration" : "', StringConversions.toString(p_invitationDuration), '"',
                                                        "}",
                                                    "}"
                                                    );

        bytes[] memory callbackFunctions = new bytes[](2);

        callbackFunctions[0] = abi.encodeWithSignature(
            "inviteCallback(address,uint256,string)", 
            invitee, 
            p_invitationDuration, 
            invitedPersonEmail
        );
        callbackFunctions[1] = abi.encodeWithSignature("devnull()");

        bytes memory callbackWhenVoting = (abi.encodeWithSignature("renewMembership()"));
        
        votings[numberVotings] = new Voting(
            textForVoting,
            binaryVotingAnswersTemplate,
            address(ownerAddress),
            callbackFunctions,
            voterTurnoutThreshold,
            callbackWhenVoting
        );

        votings[numberVotings].startVoting(p_voters, votingTime);
        numberVotings++;
    }

    function createVotingForOrganizationDetailsChange(
        address[] memory p_voters,
        string memory p_name,
        string memory p_description,
        uint p_membershipPeriod,
        uint p_recoveryPeriod,
        uint p_votingTime,
        uint p_voterTurnoutThreshold
    )
        public
        override
    {
        validateAccessRights();
        
        string memory membershipPeriodOptional =  StringConversions.toString(p_membershipPeriod);
        string memory recoveryPeriodOptional =  StringConversions.toString(p_recoveryPeriod);
        string memory votingTimeOptional = StringConversions.toString(p_votingTime);
        string memory voterTurnoutThresholdOptional = StringConversions.toString(p_voterTurnoutThreshold);

        string memory textForVoting = string.concat('{"title" : ', '"Edit organization", ',
                                                        '"parameters": {',
                                                            '"name" : "', p_name, '", ',
                                                            '"description" : "', p_description, '", ',
                                                            '"votingTime" : "', votingTimeOptional, '", ',
                                                            '"voterTurnoutThreshold" : "', voterTurnoutThresholdOptional, '", ',
                                                            '"membershipPeriod" : "', membershipPeriodOptional, '", ',
                                                            '"recoveryPeriod" : "', recoveryPeriodOptional, '"',
                                                        "}",
                                                    "}"
                                                    );

        bytes[] memory callbackFunctions = new bytes[](2);

        callbackFunctions[0] = abi.encodeWithSignature(
            "updateCallback(string,string,uint256,uint256,uint256,uint256)",
            p_name, 
            p_description, 
            p_membershipPeriod, 
            p_recoveryPeriod, 
            p_votingTime, 
            p_voterTurnoutThreshold
        );
        callbackFunctions[1] = abi.encodeWithSignature("devnull()");

        bytes memory callbackWhenVoting = abi.encodeWithSignature("renewMembership()");
        
        votings[numberVotings] = new Voting(
            textForVoting,
            binaryVotingAnswersTemplate,
            address(ownerAddress),
            callbackFunctions,
            voterTurnoutThreshold,
            callbackWhenVoting
        );

        votings[numberVotings].startVoting(p_voters, votingTime);
        numberVotings++;
    }

    function updateVotingParameters(uint p_votingTime, uint p_voterTurnoutThreshold) public {
        validateAccessRights();

        votingTime = p_votingTime;
        voterTurnoutThreshold = p_voterTurnoutThreshold;
    }

    function getVotings() public override view returns (Voting[] memory) {
        validateAccessRights();

        Voting[] memory votingsToReturn = new Voting[](numberVotings);
        for (uint i = 0; i < numberVotings; i++) {
            votingsToReturn[i] = votings[i];
        }
        return votingsToReturn;
    }

    function checkIfVotingIsAuthorisedToCallback(address sender) public override view returns (bool) {
        validateAccessRights();

        for (uint i = 0; i < numberVotings; i++) {
            if (address(votings[i]) == sender && !votings[i].checkIfVotingIsFinalized()) {
                return true;
            }
        }

        return false;
    }

    function validateAccessRights() private view {
        if (msg.sender != ownerAddress) {
            revert(NOT_OWNER_ERR_MSG);
        }
    }
}
