// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract Member {
    struct MemberDetails {
        string name;
        string surname;
        string email;
        string nickname;
        Member.Role role;
        address owner;
    }

    enum Role {
        ADMIN,
        EDITOR,
        STANDARD
    }

    string name;
    string email;
    address owner;
    string surname;
    string nickname;
    Role role;
    address organization;

    constructor(
        string memory p_name,
        string memory p_surname,
        string memory p_email,
        string memory p_nickname,
        Role p_role
    ) 
    {
        name = p_name;
        email = p_email;
        owner = tx.origin;
        surname = p_surname;
        nickname = p_nickname;
        role = p_role;
        organization = msg.sender;
    }

    function setName(string calldata p_name) public {
        if (organization == msg.sender) {
            name = p_name;
        }
    }

    function setEmail(string calldata p_email) public {
        if (organization == msg.sender) {
            email = p_email;
        }
    }

    function setSurname(string calldata p_surname) public {
        if (organization == msg.sender) {
            surname = p_surname;
        }
    }

    function setNickname(string calldata p_nickname) public {
        if (organization == msg.sender) {
            nickname = p_nickname;
        }
    }

    function setRole(Role p_role) public {
        if (organization == msg.sender) {
            role = p_role;
        }
    }
    function getMemberDetails() public view returns (MemberDetails memory) {
        return MemberDetails({
                name: getName(),
                surname: getSurname(),
                email: getEmail(),
                nickname: getNickname(),
                role: getRole(),
                owner: getOwner()
            });
    }

    function getName() public view returns (string memory) {
        return name;
    }

    function getEmail() public view returns (string memory) {
        return email;
    }

    function getOwner() public view returns (address) {
        return owner;
    }

    function getSurname() public view returns (string memory) {
        return surname;
    }

    function getNickname() public view returns (string memory) {
        return nickname;
    }

    function getRole() public view returns (Role) {
        return role;
    }
}
