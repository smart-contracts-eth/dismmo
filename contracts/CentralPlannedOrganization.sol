// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;


import "./Organization.sol";


contract CentralPlannedOrganization is Organization {

    struct OrganizationDetails{
        string name;
        string description;
        uint membershipPeriod;
        uint recoveryPeriod;
        string organizationType;
    }

    Member internal admin;

    string constant ADMIN_OPERATION_ONLY_ERR_MSG = 'Admin operation only';
    string constant ADMIN_CAN_NOT_LEAVE_ORGANIZATION_ERR_MSG = 'Admin can not leave organization before chosing new one';
    string constant ADMIN_ROLE_CAN_NOT_BE_ASSIGNED_ERR_MSG = 'Admin role can not be assigned to any member without changing admin';
    string constant ADMIN_ROLE_CANNOT_BE_CHANGED_ERR_MSG = 'Admin role can not be chenged without chaning admin';
    string constant NO_ACCESS_RIGHTS_ERR_MSG = 'Member without access rights';
    string constant NO_RIGHTS_TO_DOWNGRADE_ROLE_ERR_MSG = 'No access rights to downgrade role';
    string constant SAME_ROLE_ERR_MSG = 'The same role';
    string constant ADD_ADMIN_MEMBER_ERR_MSG = 'Member with admin role can not be added';
    string constant ALREADY_ADMIN_ERR_MSG = 'Member is already an admin';

    constructor(
        string memory p_name,
        string memory p_description,
        uint p_membershipPeriod,
        uint p_recoveryPeriod
    ) 
        Organization(p_name, p_description, p_membershipPeriod, p_recoveryPeriod)
    {
        admin = new Member("", "", "", "", Member.Role.ADMIN);
    }

    function update(
        string calldata p_name,
        string calldata p_description,
        uint p_membershipPeriod,
        uint p_recoveryPeriod
    ) 
        public 
    {
        editorOperationGuard();
        _update(p_name, p_description, p_membershipPeriod, p_recoveryPeriod);
    }

    function invite(address invitee, uint p_invitationDuration, string memory invitedPersonEmail) public {
        if (invitee == admin.getOwner()) {
            revert(MEMBER_ALREADY_EXISTS);
        }

        memberOperationGuard();
        _invite(invitee, p_invitationDuration, invitedPersonEmail);
    }

    function cancelInvitation(address invitee) public {
        memberOperationGuard();
        _cancelInvitation(invitee);
    }

    function addMember(
        string calldata p_name,
        string calldata p_surname,
        string calldata p_email,
        string calldata p_nickname
    ) 
        public 
    {
        _addMember(p_name, p_surname, p_email, p_nickname, Member.Role.STANDARD);
    }

    function setMemberEmail(string calldata p_email) public {
        memberOperationGuard();
        _setMemberEmail(p_email);
    }

    function setMemberRole(address wallet, Member.Role p_role) public {
        editorOperationGuard();

        if (p_role == Member.Role.ADMIN) {
            revert(ADMIN_ROLE_CAN_NOT_BE_ASSIGNED_ERR_MSG);
        }
        if (admin.getOwner() == wallet) {
            revert(ADMIN_ROLE_CANNOT_BE_CHANGED_ERR_MSG);
        }

        address memberAddr = _getMemberByWallet(wallet);

        if (memberAddr == NULL_ADDRESS) {
            revert(MEMBER_NOT_EXIST);
        }

        Member memberToModify = Member(memberAddr);
        if (memberToModify.getRole() == p_role) {
            revert(SAME_ROLE_ERR_MSG);
        }

        if (tx.origin == admin.getOwner()) {
            setMemberRoleByAdmin(memberToModify, p_role);
        } else {
            setMemberRoleByEditor(memberToModify, p_role);
        }
    }

    function changeAdmin(address wallet) public{
        adminOperationGuard();

        if (admin.getOwner() == wallet) {
            revert(ALREADY_ADMIN_ERR_MSG);
        }

        address memberAddr = _getMemberByWallet(wallet);

        if (memberAddr == NULL_ADDRESS) {
            revert(MEMBER_NOT_EXIST);
        }

        Member member = Member(memberAddr);
        admin.setRole(Member.Role.STANDARD);
        member.setRole(Member.Role.ADMIN);

        _addMember(admin);  // add prev admin to a members list
        _deleteMember(memberAddr); // remove new admin from a members list
        admin = member;
    }

    function updateMember(string calldata p_name, string calldata p_surname, string calldata p_nickname) public {
        memberOperationGuard();
        _updateMember(p_name, p_surname, p_nickname);
    }

    function updateAdmin(string calldata p_name, string calldata p_surname, string calldata p_nickname) public {
        adminOperationGuard();
        admin.setName(p_name);
        admin.setSurname(p_surname);
        admin.setNickname(p_nickname);
    }

    function deleteMember(address m) public {
        adminOperationGuard();
        _deleteMember(m);
    }

    function leaveOrganization() public {
        memberOperationGuard();

        // Admin cannot leave the organization, changeAdmin function is needed before leaving
        if (admin.getOwner() == tx.origin) {
            revert(ADMIN_CAN_NOT_LEAVE_ORGANIZATION_ERR_MSG);
        }

        address member_addr = _getMemberByWallet(tx.origin);
        _deleteMember(member_addr);
    }

    function renewMembership() public {
        memberOperationGuard();
        _renewMembership();
    }

    function getOrganizationDetails() public view returns (OrganizationDetails memory) {
        memberOperationGuard();

        return OrganizationDetails({
            name: name,
            description: description,
            membershipPeriod: membershipPeriod,
            recoveryPeriod: recoveryPeriod,
            organizationType: getOrganizationType()
        });
    }

    function getAdmin() public view returns(address) {
        memberOperationGuard();
        return address(admin);
    }

        function getMembers() public view returns(Member[] memory) {
        memberOperationGuard();
        return _getMembers();
    }

    function getMember() public view returns(address) {
        memberOperationGuard();
        return _getMember();
    }

    function checkIfMembershipExpired(Member member) public view returns(bool) {
        memberOperationGuard();
        return _checkIfMembershipExpired(member);
    }

    function getExpiringMembershipsEmails() public view returns (string[] memory) {
        return _getExpiringMembershipsEmails();
    }

    function getExpiringMembershipsEmailsOneDayBeforeExpiring() public view returns (string[] memory) {
        return _getExpiringMembershipsEmailsOneDayBeforeExpiring();
    }

    function checkIfMemberBelongsToOrganization() public view returns(bool) {
        if (tx.origin == admin.getOwner()) {
            return true;
        } else {
            return _checkIfMemberBelongsToOrganization(tx.origin);
        }
    }

    function getMembershipStartTime() public view returns(uint) {
        memberOperationGuard();
        return _getMembershipStartTime();
    }

    function checkIfInvited(address person) public  view returns(bool) {
        return _checkIfInvited(person);
    }

    function getOrganizationType() public pure returns (string memory) {
        return "CENTRAL_PLANNED_ORGANIZATION";
    }

    function setMemberRoleByAdmin(Member memberToModify, Member.Role p_role) private {
        // admin privileges: standard -> editor; editor -> standard

        memberToModify.setRole(p_role);
    }

    function setMemberRoleByEditor(Member memberToModify, Member.Role p_role) private {
        // editor privileges: standard -> editor; own_role -> standard

        Member.Role memberToModifyRole = memberToModify.getRole();
        if (memberToModify.getOwner() == tx.origin || memberToModifyRole == Member.Role.STANDARD) {
            memberToModify.setRole(p_role);
        } else {
            revert(NO_RIGHTS_TO_DOWNGRADE_ROLE_ERR_MSG);
        }
    }

    function adminOperationGuard() private view {
        if (admin.getOwner() != tx.origin) {
            revert(ADMIN_OPERATION_ONLY_ERR_MSG);
        }
    }

    function editorOperationGuard() private view {
        Member member = Member(getMember());
        if (tx.origin != admin.getOwner() && member.getRole() != Member.Role.EDITOR) {
            revert(NO_ACCESS_RIGHTS_ERR_MSG);
        }
    }

    function memberOperationGuard() private view {
        if (!_checkIfMemberBelongsToOrganization(tx.origin) && tx.origin != admin.getOwner()) {
            revert(NOT_MEMBER_REQUEST_ERR_MSG);
        }
    }
}
