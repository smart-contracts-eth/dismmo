from brownie import CentralPlannedOrganization, accounts

def main():
    acct = accounts.load('metamask-account')
    CentralPlannedOrganization.deploy("Test organization", "For test purpose", 1000000, 1000000, {'from': acct, 'gas_limit':9999999, 'allow_revert':True})
