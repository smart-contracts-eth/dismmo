from brownie import Contract, accounts
import json

def main():
    accounts.load('metamask-account')

    contract_name = "DecentralizedAutonomousOrganization"
    contract_address = "0x841994C958506385Fc2d8e3246B4D4bB6645E284"

    inviteeFW = "0x20f87Ff207e9c208B7D49665477A7CB2df838202"
    inviteeKK = "0x2D3A2dbC78b2394B5EA3D4c07d7688281ce4Ba43"
    # Load the ABI from the JSON file for the specified network
    with open(f"build/contracts/{contract_name}.json") as f:
        abi = json.load(f)["abi"]

    # Create a contract instance
    your_contract = Contract.from_abi(contract_name, contract_address, abi)
    
    acct = accounts.load('metamask-account')
    
    print(your_contract.invite(inviteeFW, 10000000, "x@mail.com", {'from':acct, 'gas_limit':10000000, 'allow_revert':True}))
    