from brownie import *
from brownie.network import accounts
from argparse import ArgumentParser
import json


DEFAULT_GAS_LIMIT = 9e6


def create_parser():
    parser = ArgumentParser()
    parser.add_argument('--config', type=str, required=True)
    parser.add_argument('--proxy_address', type=str, required=False)
    parser.add_argument('--gas_limit', type=int, required=False)
    args = parser.parse_args()

    return args


def load_config(config_path):
    print(f'Loading {config_path}...')
    with open(config_path, "r") as f:
        f_config = f.read()

    config = json.loads(f_config)

    return config
    

def load_brownie(network_id, account_id):
    print("Loading brownie...")
    p = project.load('.', name="dismmo")
    p.load_config()

    print("Connecting to network...")
    network.connect(network_id)
    
    print("Loading account")
    acct = accounts.load(account_id)

    return p, acct
    

def deploy_proxy_contract(account_addr):
    from brownie.project.dismmo import OrganizationProxy
    return OrganizationProxy.deploy({'from': account_addr, 'gas_limit':1e6, 'allow_revert':True})


def deploy_organization_contract(p, organization_type, organization_parameters, acct, gas_limit, proxy_address=None):
    from brownie.project.dismmo import DecentralizedAutonomousOrganization, CentralPlannedOrganization, OrganizationProxy
    
    def updateProxy(contract_addr):
        if proxy_address:
            proxy_contract = OrganizationProxy.at(proxy_address)
            proxy_contract.updateOrganizationAddress(contract_addr.address, {'from': acct, 'gas_limit': gas_limit, 'allow_revert':True})

    if (organization_type == 'CentralPlannedOrganization'):
        print(f'Deploying {organization_type}...')

        name = organization_parameters['name']
        description = organization_parameters['description']
        membership_period = int(organization_parameters['membership_period'])
        recovery_reriod = int(organization_parameters['recovery_reriod'])

        cpo_contract = CentralPlannedOrganization.deploy(name, description, membership_period, recovery_reriod, {'from': acct, 'gas_limit': gas_limit, 'allow_revert':True})
        updateProxy(cpo_contract)

    elif (organization_type == 'DecentralizedAutonomousOrganization'):
        print(f'Deploying {organization_type}...')
        
        name = organization_parameters['name']
        description = organization_parameters['description']
        membership_period = int(organization_parameters['membership_period'])
        recovery_reriod = int(organization_parameters['recovery_reriod'])
        voting_time = int(organization_parameters['voting_time'])
        voter_turnout_threshold = int(organization_parameters['voter_turnout_threshold'])
        print(name, description, membership_period, recovery_reriod, voting_time, voter_turnout_threshold)
        
        dao_contract = DecentralizedAutonomousOrganization.deploy(name, description, membership_period, recovery_reriod, voting_time, voter_turnout_threshold, {'from': acct, 'gas_limit':gas_limit, 'allow_revert':True})
        updateProxy(dao_contract)

    else:
        print(f'Unknown organization type: {organization_type}; check configuration file')
    

def main():
    args = create_parser()
    config = load_config(args.config)
    p, acct = load_brownie(config["network"], config["account"])

    if args.proxy_address is None:
        proxy_address = deploy_proxy_contract(acct).address
    else:
        proxy_address = args.proxy_address

    gas_limit = args.gas_limit if (args.gas_limit is not None) else DEFAULT_GAS_LIMIT
    deploy_organization_contract(p, config['organization_type'], config["parameters"], acct, proxy_address=proxy_address, gas_limit=gas_limit)
    print(f'proxy contract with organization deployed at: {proxy_address}')
    

if __name__ == "__main__":
    main()
