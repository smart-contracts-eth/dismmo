from brownie import DecentralizedAutonomousOrganization, accounts

def main():
    acct = accounts.load('metamask-account')
    DecentralizedAutonomousOrganization.deploy("Test DAO organization", "For test purpose", 1000000, 1000000, 600000, 51, {'from': acct, 'gas_limit':9999990, 'allow_revert':True})